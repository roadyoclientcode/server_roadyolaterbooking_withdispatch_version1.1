<!DOCTYPE html>
<html lang="en">
<head>        
    <title>Taurus</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />        
    
    <script type='text/javascript' src='js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script>   
    <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
    <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>
        
    <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>
    <script type='text/javascript' src='js/plugins/tinymce/tinymce.min.js'></script>
    
    <script type='text/javascript' src='js/plugins.js'></script>    
    <script type='text/javascript' src='js/actions.js'></script>
    <script type='text/javascript' src='js/charts.js'></script>
</head>
<body class="bg-img-num1"> 
    
    <div class="page-container">
        <div class="page-sidebar">
            <div class="page-navigation-panel logo"></div>
            <div class="page-navigation-panel">
               <!-- <div class="name">Howdy, Aqvatarius</div>-->
                <div class="control"><a href="#" class="psn-control"><span class="icon-reorder"></span></a></div>
            </div>            
            <div class="page-navigation-panel search">                                
                <div class="input-group">                        
                    <!-- <input type="text" class="form-control" placeholder="Search..."/>-->
                   <!--   <div class="input-group-addon"><span class="icon-search"></span></div>-->
                </div>                
            </div>
            <ul class="page-navigation">
                <li>
                    <a href="index.html">
                        <span class="icon-home"></span> USERS
                    </a>
                </li>                            
                <li>
                    <a href="#"><span class="icon-pencil"></span>PROGRAMS</a>
                    <ul>                                    
                        <li><a href="form_elements.html">SCHEDULE</a></li>
                        
                     
                    </ul>                                
                </li>
               <!-- <li>
                    <a href="#"><span class="icon-cogs"></span> Components</a></li> 
                    <ul>
                        <li><a href="component_blocks.html">Blocks and panels</a></li></li> 
                        <li><a href="component_buttons.html">Buttons</a></li></li> 
                        <li><a href="component_modals.html">Modals and popups</a></li></li>                                    
                        <li><a href="component_tabs.html">Tabs, accordion, selectable, sortable</a></li>
                        </li>
                        <li><a href="component_progress.html">Progressbars</a></li>
                        <li><a href="component_lists.html">List groups</a></li>
                        <li><a href="component_messages.html">Messages</a></li>                                    
                        <li>
                            <a href="#">Tables<i class="icon-angle-down pull-right"></i></a>
                            <ul>
                                <li><a href="component_table_default.html">Default tables</a></li>
                                <li><a href="component_table_sortable.html">Sortable tables</a></li>                                            
                            </ul>
                        </li> -->                                                                        
                      <!--  <li>
                            <a href="#">Layouts<i class="icon-angle-down pull-right"></i></a>
                            <ul>
                                <li><a href="component_layout_blank.html">Default layout(blank)</a></li>
                                <li><a href="component_layout_custom.html">Custom navigation</a></li>
                                <li><a href="component_layout_scroll.html">Content scroll</a></li>
                                <li><a href="component_layout_fixed.html">Fixed content</a></li>
                                <li><a href="component_layout_white.html">White layout</a></li>
                            </ul>
                        </li>
                        <li><a href="component_charts.html">Charts</a></li>
                        <li><a href="component_maps.html">Maps</a></li>
                        <li><a href="component_typography.html">Typography</a></li>
                        <li><a href="component_gallery.html">Gallery</a></li>
                        <li><a href="component_calendar.html">Calendar</a></li>
                        <li><a href="component_icons.html">Icons</a></li>                                    
                    </ul>
                </li>                          
                <li><a href="widgets.html"><span class="icon-globe"></span> Widgets</a></li>
                <li>
                    <a href="#"><span class="icon-file-alt"></span> Pages</a>
                    <ul>
                        <li><a href="sample_login.html">Login</a></li>
                        <li><a href="sample_registration.html">Registration</a></li>
                        <li><a href="sample_profile.html">User profile</a></li>
                        <li><a href="sample_profile_social.html">Social profile</a></li>
                        <li><a href="sample_edit_profile.html">Edit profile</a></li>
                        <li><a href="sample_mail.html">Mail</a></li>
                        <li><a href="sample_search.html">Search</a></li>
                        <li><a href="sample_invoice.html">Invoice</a></li>
                        <li><a href="sample_contacts.html">Contacts</a></li>
                        <li><a href="sample_tasks.html">Tasks</a></li>
                        <li><a href="sample_timeline.html">Timeline</a></li>
                        <li>
                            <a href="#">Email templates<i class="icon-angle-down pull-right"></i></a>
                            <ul>
                                <li><a href="email_sample_1.html">Sample 1</a></li>
                                <li><a href="email_sample_2.html">Sample 2</a></li>
                                <li><a href="email_sample_3.html">Sample 3</a></li>
                                <li><a href="email_sample_4.html">Sample 4</a></li>
                            </ul>
                        </li> -->                                   
                       <!-- <li>
                            <a href="#">Error pages<i class="icon-angle-down pull-right"></i></a>
                            <ul>
                                <li><a href="sample_error_403.html">403 Forbidden</a></li>
                                <li><a href="sample_error_404.html">404 Not Found</a></li>
                                <li><a href="sample_error_500.html">500 Internal Server Error</a></li>
                                <li><a href="sample_error_503.html">503 Service Unavailable</a></li>
                                <li><a href="sample_error_504.html">504 Gateway Timeout</a></li>                                                                                       
                            </ul>
                        </li> -->                                   
                    </ul>
                </li>                
                
            </ul>

        </div>
        <div class="page-content page-content-white">
            
            <div class="container">        
                
                <div class="row">
                    
                 <div class="content">

                        <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall"/></th>
                                    <th width="25%">USER ID</th>
                                    <th width="25%">FIRST Name</th>
                                    <th width="25%">LAST Name</th>
                                    <th width="25%">E-mail</th>
                                    <th width="25%">STATUS</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>101</td>
                                    <td>Dmitry</td>
                                    <td>dmitry@domain.com</td>
                                    <td>+98(765)432-10-98</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>102</td>
                                    <td>Alex</td>
                                    <td>alex@domain.com</td>
                                    <td>+98(765)432-10-99</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>103</td>
                                    <td>John</td>
                                    <td>john@domain.com</td>
                                    <td>+98(765)432-10-97</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>104</td>
                                    <td>Angelina</td>
                                    <td>angelina@domain.com</td>
                                    <td>+98(765)432-10-90</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>105</td>
                                    <td>Tom</td>
                                    <td>tom@domain.com</td>
                                    <td>+98(765)432-10-92</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>106</td>
                                    <td>Helen</td>
                                    <td>helen@domain.com</td>
                                    <td>+98(765)432-11-33</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>107</td>
                                    <td>Aqvatarius</td>
                                    <td>aqvatarius@domain.com</td>
                                    <td>+98(765)432-15-66</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>108</td>
                                    <td>Olga</td>
                                    <td>olga@domain.com</td>
                                    <td>+98(765)432-11-97</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>109</td>
                                    <td>Homer</td>
                                    <td>homer@domain.com</td>
                                    <td>+98(765)432-11-90</td>                                    
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="checkbox"/></td>
                                    <td>110</td>
                                    <td>Tifany</td>
                                    <td>tifany@domain.com</td>
                                    <td>+98(765)432-11-92</td>                                    
                                </tr>                                
                            </tbody>
                        </table>                                        

                    </div>
                </div>
                
            </div>            
            
        </div>
    </div>
    
</body>
</html>

