<?php
error_reporting(0);
session_start();
include('../../Models/ConDB.php');
$db1 = new ConDB();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}


if (isset($_GET['mas_id'])) 
{
    $_SESSION['admin_id'] = $_GET['mas_id'];
}
?>

<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 11, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null]});
    });
</script>
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">
    <?php
    if ($status == '2') {
        ?>

        <div style="font-size:20px;"> ACCEPTED VEHICLE</div>
        <?php
    }
    ?>
    <?php
    if ($status == '4') {
        ?>

        <div style="font-size:20px;"> REJECTED VEHICLE</div>
        <?php
    }
    ?>
    <?php
    if ($status == '6') {
        ?>

        <div style="font-size:20px;"> LIVE BUT NOT BOOKED</div>
        <?php
    }
    ?>
    <?php
    if ($status == '5') {
        ?>

        <div style="font-size:20px;">FREE VEHICLE</div>
        <?php
    }
    ?>
    <?php
    if ($status == '7') {
        ?>

        <div style="font-size:20px;">LIVE AND BOOKED</div>
        <?php
    }
    ?>
    <?php
    if ($status == '8') {
        ?>

        <div style="font-size:20px;">OFFLINE VEHICLES</div>
        <?php
    }
    ?>
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '4') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="2" data-msg="active">ACCEPTED</button>    
            <?php
        }
        if ($status == '1' || $status == '2') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="4" data-msg="inactive">REJECTED</button>
            <?php
        }
        ?></div>
    <div style="float:none;"></div>
    <!-- <a href="#" class="widget-icon widget-icon-dark" id="buttonClass" style=" color: rgb(247, 0, 0); background-color: blue;float: right;
        margin-Right: 80px;"><span class="icon-trash"></span></a>-->

    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>
                <th width="8%">VEHICLE ID</th>
                <th width="8%">TITLE</th>

                <th width="8%">VEHICLE MODEL</th>
                <th width="8%">VEHICLE TYPE</th> 
                <th width="8%">VEHICLE REG NO</th>
                <th width="8%">LICENCE PLAT NO</th> 
                <th width="8%">VEHICLE SEATING</th>
                <th width="8%">VEHICLE COLOR</th>   

                <th width="8%">COMPANY</th>  
                <!--<th width="8%">STATUS</th>-->
                <th width="8%">SELECT </th>
            </tr>
        </thead>
        <tbody style="font-size: 12px;">

            <?php
            $logedIN = ' and u.loggedIN = 1 ';

            $type = ',u.type as type';

            if ($status == '6') {
                $logedIN = $type = '';
            }

            if ($status == '5')
                $logedIN = '';

//            if ($status == '2')
//                $status = "1,2";


            $mongo = $db1->mongo;
            $location = $mongo->selectCollection('location');

            if ($status == '8') {
                $user_data = $location->find(array('status' => array('$in' => array(4, 5)), 'carId' => array('$gt' => 0)));
                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.workplace_id IN (" . rtrim($car_query, ',') . ") and w.company in(".$_SESSION['admin_id'].") order by w.workplace_id DESC";
            } else if ($status == '6' || $status == '7') {

                if ($status == '6')
                    $user_data = $location->find(array('status' => 3, 'inBooking' => 1));//.limit( 50 );//, 'inBooking' => 1));

                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                if ($status == '6')
                    $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 1 and w.workplace_id IN (" . rtrim($car_query, ',') . ") and w.company in(".$_SESSION['admin_id'].") order by w.workplace_id DESC";
               else
                    $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 1 and w.workplace_id IN (select distinct car_id from appointment where status IN (6,7,8)) and w.company in(".$_SESSION['admin_id'].")order by w.workplace_id DESC";
            } else if ($status == '2') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (1,2) and w.company in(".$_SESSION['admin_id'].") order by w.workplace_id DESC";
            } else if ($status == '4') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (4) and w.company in(".$_SESSION['admin_id'].") order by w.workplace_id DESC";
            } else if ($status == '5') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (2) and w.company in(".$_SESSION['admin_id'].") order by w.workplace_id DESC";
            } else {
                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 4 and w.company in(".$_SESSION['admin_id'].") order by w.workplace_id DESC";
            }
            $result1 = mysql_query($accQry, $db1->conn);
                // echo $accQry;
            $i = 1;
           
          //  echo $_SESSION['admin_id'];
            
            while ($row = mysql_fetch_assoc($result1)) {
                ?>


                
                <tr id="doc_rows<?php echo $i; ?>">
                    <td><?php echo $row['workplace_id'] ?></td>
                    <td><a target="_blank" href="" data="<?php echo $i; ?>" data-toggle="modal"> 
                            <?php
                            echo $row['vehicletype'];
                            ?>
                        </a></td>
                    <td>
                        <?php
                        echo $row['vehiclemodel'];
                        ?>
                    </td>
                    <td>

                        <?php
                        echo $row['type_name'];
                        ?>
                    </td>
                    <td><?Php echo $row['Vehicle_Reg_No']; ?></td>
                    <td><?Php echo $row['License_Plate_No']; ?></td>
                    <td><?Php echo $row['Vehicle_seating']; ?></td>
                    <td><?Php echo $row['Vehicle_Color']; ?></td>

                    <td><?php echo $row['companyname']; ?></td>
                    <td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['workplace_id']; ?>" style="background: white;height: 20px;width: 12px;" /></td>
                </tr>
                <?php
                $i++;
            }
            ?> 

        </tbody>
    </table>               
    <!--/<?php echo $accQry.'---'.$car_query;?>-->
</div> 

<script type="text/javascript">
    $(document).ready(function() {
//            alert('1');
        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });


        $('#change_pass_doc').click(function() {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {mas_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#ActiveButton,#RejectButton').click(function() {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one doctor in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 8, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function(result) {
                       // alert(result.message);
                       // alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function() {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });

    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


