<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../Models/ConDB.php';
$db = new ConDB();

$selectAllCityDataQry = "select type_id from workplace_types where city_id = '" . $_REQUEST['item_list'] . "'";
$selectAllCityDataRes = mysql_query($selectAllCityDataQry, $db->conn);

$types = array();
$affectedRows = 0;

while ($type = mysql_fetch_assoc($selectAllCityDataRes)) {
    $types[] = (int) $type['type_id'];
}

$deleteAllVehicles = "delete from workplace where type_id IN (" . implode(",", $types) . ")";
mysql_query($deleteAllVehicles, $db->conn);
$affectedRows += mysql_affected_rows();

$deleteCity = "delete from city_available where city_id = '" . $_REQUEST['item_list'] . "'";
mysql_query($deleteCity, $db->conn);
$affectedRows += mysql_affected_rows();

$deleteCoupons = "delete from coupons where city_id = '" . $_REQUEST['item_list'] . "'";
mysql_query($deleteCoupons, $db->conn);
$affectedRows += mysql_affected_rows();

$deleteVehicleTypes = "delete from workplace_types where type_id IN (" . implode(",", $types) . ")";
mysql_query($deleteVehicleTypes, $db->conn);
$affectedRows += mysql_affected_rows();

$vehicleTypes = $db->mongo->selectCollection('vehicleTypes');
$location = $db->mongo->selectCollection('location');

$return[] = $vehicleTypes->remove(array('type' => array('$in' => $types)));

$getAllDriversCursor = $location->find(array('type' => array('$in' => $types)));

$mas_id = array();

foreach ($getAllDriversCursor as $driver) {
    $mas_id[] = (int) $driver['user'];
}

$return[] = $location->update(array('user' => array('$in' => $mas_id)), array('$set' => array('type' => 0, 'carId' => 0, 'status' => 4)));

$updateMysqlDriverQry = "update master set type_id = '',workplace_id = '' where mas_id in (" . implode(',', $mas_id) . ")";
mysql_query($updateMysqlDriverQry, $db->conn);
$affectedRows += mysql_affected_rows();

echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'City Deleted'));
?>
