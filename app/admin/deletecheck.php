<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../Models/ConDB.php';
$db = new ConDB();

if ($_REQUEST['type'] == '2') {//company delete
    $affectedRows = 0;

    $deleteVehicleTypes = "delete from company_info where company_id = " . $_REQUEST['item_list'];
    mysql_query($deleteVehicleTypes, $db->conn);
    $affectedRows += mysql_affected_rows();

    if ($affectedRows <= 0) {

        echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
        return false;
    }

    $deleteAllVehicles = "delete from workplace where company = '" . $_REQUEST['item_list'] . "'";
    mysql_query($deleteAllVehicles, $db->conn);
    $affectedRows += mysql_affected_rows();

    $selectType = "select mas_id from master where company_id = '" . $_REQUEST['item_list'] . "'";
    $selectTypeRes = mysql_query($selectType, $db->conn);

    $mas_id = array();

    while ($type = mysql_fetch_assoc($selectTypeRes)) {
        $mas_id[] = (int) $type['mas_id'];
    }

//    $deleteAllVehicles = "delete from workplace_types where type_id  in (" . implode(',', $type_ids) . ")";
//    mysql_query($deleteAllVehicles, $db->conn);
//    $affectedRows += mysql_affected_rows();


    $location = $db->mongo->selectCollection('location');

    $return[] = $location->remove(array('user' => array('$in' => $mas_id)));

    $updateMysqlDriverQry = "delete from master where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlDriverQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlApptQry = "delete from appointment where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlApptQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    echo json_encode(array('flag' => 0, 'affectedRows' => $deleteAllVehicles . $deleteVehicleTypes . $updateMysqlDriverQry, 'msg' => 'company deleted successfully'));
} else if ($_REQUEST['type'] == '1') { // vehicle type delete
    $affectedRows = 0;

    $deleteAllVehicles = "delete from workplace_types where type_id  = '" . $_REQUEST['item_list'] . "'";

    mysql_query($deleteAllVehicles, $db->conn);
    $affectedRows += mysql_affected_rows();

    if ($affectedRows <= 0) {

        echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
        return false;
    }

    $deleteAllVehicles = "delete from workplace where type_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($deleteAllVehicles, $db->conn);
    $affectedRows += mysql_affected_rows();


    $vehicleTypes = $db->mongo->selectCollection('vehicleTypes');
    $location = $db->mongo->selectCollection('location');

    $return[] = $vehicleTypes->remove(array('type' => (int) $_REQUEST['item_list']));

    $getAllDriversCursor = $location->find(array('type' => (int) $_REQUEST['item_list']));

    $mas_id = array();

    foreach ($getAllDriversCursor as $driver) {
        $mas_id[] = (int) $driver['user'];
    }

    $return[] = $location->remove(array('user' => array('$in' => $mas_id)));

    $updateMysqlDriverQry = "delete from master where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlDriverQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlApptQry = "delete from appointment where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlApptQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Vehicle type deleted.'));
} else if ($_REQUEST['type'] == '3') { // Driver delete
    $affectedRows = 0;

    $getMasterDet = "select * from master where mas_id = '" . $_REQUEST['item_list'] . "'";
    $getMasterRes = mysql_query($getMasterDet, $db->conn);
    $masDet = mysql_fetch_assoc($getMasterRes);

    if (!is_array($masDet)) {

        echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Driver not available'));
        return false;
    }
    $location = $db->mongo->selectCollection('location');

    $location->remove(array('user' => (int) $_REQUEST['item_list']));

    $updateCarQry = "update workplace set status = 2 where workplace_id = '" . $masDet['car_id'] . "'";
    mysql_query($updateCarQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlDriverQry = "delete from master where mas_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateMysqlDriverQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $deleteDriverDocs = "delete from docdetail where driverid = '" . $_REQUEST['item_list'] . "'";
    mysql_query($deleteDriverDocs, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlApptQry = "delete from appointment where mas_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateMysqlApptQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from passenger_rating where mas_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from master_ratings where mas_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from user_sessions where user_type = 1 and oid = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Driver deleted.'));
} else if ($_REQUEST['type'] == '4') { // vehicle delete
    $affectedRows = 0;

    $selectCars = "select appointment_id from appointment where car_id = '" . $_REQUEST['item_list'] . "'";
    $selectCarsRes = mysql_query($selectCars, $db->conn);
    $apptIDs = array();
    while ($type = mysql_fetch_assoc($selectCarsRes)) {
        $apptIDs[] = (int) $type['appointment_id'];
    }

    $getMasterDet = "select mas_id from master where workplace_id = '" . $_REQUEST['item_list'] . "'";
    $getMasterRes = mysql_query($getMasterDet, $db->conn);
    $masDet = mysql_fetch_assoc($getMasterRes);

    if (is_array($masDet)) {

        $location = $db->mongo->selectCollection('location');

        $location->update(array('carId' => (int) $_REQUEST['item_list']), array('$set' => array('type' => 0, 'carId' => 0, 'status' => 4)), array('multiple' => 1));

        mysql_query("update master set type_id = 0 and workplace_id = 0 where mas_id = '" . $masDet['mas_id'] . "'", $db->conn);
    }

    $updateCarQry = "delete from workplace where workplace_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateCarQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $deleteVehicleDocs = "delete from vechiledoc where vechileid = '" . $_REQUEST['item_list'] . "'";
    mysql_query($deleteVehicleDocs, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlApptQry = "delete from appointment where appointment_id in (" . implode(',', $apptIDs) . ")";
    mysql_query($updateMysqlApptQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from passenger_rating where appointment_id in (" . implode(',', $apptIDs) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from master_ratings where appointment_id in (" . implode(',', $apptIDs) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from user_sessions where user_type = 1 and oid = '" . $masDet['mas_id'] . "'";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Vehicle deleted.'));
} else if ($_REQUEST['type'] == '5') { // logout driver
    $selectWorkplace = "select workplace_id from master where mas_id = '" . $_REQUEST['item_list'] . "'";
    $selectIdRes = mysql_query($selectWorkplace, $db->conn);

    $id = mysql_fetch_assoc($selectIdRes);

    $location = $db->mongo->selectCollection('location');

    $removeSessionsQry = "update user_sessions set loggedIn = 2 where oid = '" . $_REQUEST['item_list'] . "' and user_type = 1 and loggedIn = 1";
    mysql_query($removeSessionsQry, $db->conn);

    $updateWorkplaceIdQry = "update master set workplace_id = '' where mas_id = '" . $_REQUEST['item_list'] . "'";
    mysql_query($updateWorkplaceIdQry, $db->conn);

    $updateWorkplaceQry = "update workplace set Status = 2 where workplace_id = '" . $id['workplace_id'] . "'";
    mysql_query($updateWorkplaceQry, $db->conn);

    $location->update(array('user' => (int) $_REQUEST['item_list']), array('$set' => array('status' => 4, 'type' => 0, 'carId' => 0, 'chn' => '', 'listner' => '')));
    echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'message' => 'Driver logged out.'));
} else if ($_REQUEST['type'] == '6') { // country delete
    $affectedRows = 0;

    $selectCities = "select City_Id from city_available where Country_Id = '" . $_REQUEST['item_list'] . "'";
    $selectCitiesRes = mysql_query($selectCities, $db->conn);

    $cities = array();

    while ($city = mysql_fetch_assoc($selectCitiesRes)) {
        $cities[] = $city['City_Id'];
    }

    $deleteCountry = "delete from country where Country_Id = '" . $_REQUEST['item_list'] . "'";
    $deleteCountryRes = mysql_query($deleteCountry, $db->conn);
    $affectedRows += mysql_affected_rows();

    if ($affectedRows <= 0) {

        echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
        return false;
    }

    $deleteCities = "delete from city_available where City_Id IN (" . implode(',', $cities) . ")";
    $deleteCitiesRes = mysql_query($deleteCities, $db->conn);
    $affectedRows += mysql_affected_rows();

    $deleteCoupons = "delete from coupons where city_id IN (" . implode(",", $cities) . ")";
    mysql_query($deleteCoupons, $db->conn);
    $affectedRows += mysql_affected_rows();

    $selectCompanies = "select company_id from company_info where city IN (" . implode(',', $cities) . ")";
    $selectCompaniesRes = mysql_query($selectCompanies, $db->conn);

    $companies = array();
    while ($company = mysql_fetch_assoc($selectCompaniesRes)) {
        $companies[] = $company['company_id'];
    }




    $deleteVehicleTypes = "delete from company_info where city in (" . implode(',', $cities) . ")";
    mysql_query($deleteVehicleTypes, $db->conn);
    $affectedRows += mysql_affected_rows();



    $selectType = "select type_id from workplace where company in (" . implode(',', $companies) . ")";
    $selectTypeRes = mysql_query($selectType, $db->conn);

    $type_ids = array();

    while ($type = mysql_fetch_assoc($selectTypeRes)) {
        $type_ids[] = (int) $type['type_id'];
    }


    $deleteAllVehicles = "delete from workplace_types where type_id  in (" . implode(',', $type_ids) . ")";
    mysql_query($deleteAllVehicles, $db->conn);
    $affectedRows += mysql_affected_rows();



    $deleteAllVehicles = "delete from workplace where type_id  in (" . implode(',', $type_ids) . ")";
    mysql_query($deleteAllVehicles, $db->conn);
    $affectedRows += mysql_affected_rows();


    $vehicleTypes = $db->mongo->selectCollection('vehicleTypes');
    $location = $db->mongo->selectCollection('location');



    $return[] = $vehicleTypes->remove(array('type' => array('$in' => $type_ids)));

    $getAllDriversCursor = $location->find(array('type' => array('$in' => $type_ids)));

    $mas_id = array();

    foreach ($getAllDriversCursor as $driver) {
        $mas_id[] = (int) $driver['user'];
    }

    $return[] = $location->remove(array('user' => array('$in' => $mas_id)));

    $updateMysqlDriverQry = "delete from master where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlDriverQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlApptQry = "delete from appointment where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlApptQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    $updateMysqlReviewQry = "delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")";
    mysql_query($updateMysqlReviewQry, $db->conn);
    $affectedRows += mysql_affected_rows();

    echo json_encode(array('flag' => 0, 'affectedRows' => $deleteAllVehicles . $deleteVehicleTypes . $updateMysqlDriverQry, 'msg' => 'Country deleted'));
}
?>
