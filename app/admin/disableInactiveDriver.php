<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../Models/ConDB.php';
$db = new ConDB();
$mongo = $db->mongo;

$currTs = time() - (2 * 60 * 60); //disable all drivers who are inactive with in half an hour

echo 'Ts:' . $currTs . '/n';

$location = $mongo->selectCollection('location');

echo $location->update(array('lastTs' => array('$lte' => $currTs), 'status' => 3), array('$set' => array('status' => 4)), array('multiple' => 1));

echo '/n';
?>
