<?php
session_start();
$m = new Mongo();
$db = $m->selectDB('PriveMd');
$collection = new MongoCollection($db, 'locationsmasternews');
$cursor = $collection->find();
$countsize = $cursor->count();
$docArr = array();
if (isset($_REQUEST['type']))
{
    $status = $_REQUEST['type'];
} 
else 
{
    $status = '1';
}
if(isset($_REQUEST['create']))
{
$to_time = strtotime($_REQUEST['enddatetime']);
$from_time = strtotime($_REQUEST['starttime']);
$total_min_slots=round(abs($to_time - $from_time) / 60,2);
$number_of_slot=round($total_min_slots/60);
$get_latlng=$collection->findOne(array("_id"=> new MongoId($_REQUEST['location'])));
if($number_of_slot > 0){
//echo '1';
$insertQry = array();
$daysArr = explode(',',$_REQUEST['dates']);
//echo '1';
$ltlng = new MongoCollection($db, 'slotbooking');
foreach ($daysArr as $day) {
//print_r($day);
	$start = strtotime($day.' '.$_REQUEST['starttime'].':00');
	$final = strtotime($day.' '.$_REQUEST['enddatetime'].':00') - (60*60);
	while ($start <= $final) {
	$next = $start + (60*60);
    $insertQry = array("doc_id"=>$_SESSION['admin_id'],'locId'=>(string)$get_latlng['_id'],"locName"=>$get_latlng['locName'],"start"=>$start,"end"=>$next,"loc"=>array("longitude"=>(double)$get_latlng['loc'][0]['longitude'],"latitude"=>(double)$get_latlng['loc'][0]['latitude']));
	$ltlng->insert($insertQry);
    $start = $start + (60*60);
    }
   }
//print_r($insertQry);
}
}
?>
<head>        
        <title>ADD LOCATION</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />        
      <!--  <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script> -->
        <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
        <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='js/Plugins_7.js'></script>
        <script type='text/javascript' src='js/plugins.js'></script>    
        <script type='text/javascript' src='js/actions.js'></script>
        <script type='text/javascript' src='js/settings.js'></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		
		<script type="text/javascript" src="js/jquery.ui.core.js"></script>
		<script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
		<!-- script type="text/javascript" src="js/jquery.ui.datepicker-es.js"></script -->
		
		<!-- loads mdp -->
		<script type="text/javascript" src="jquery-ui.multidatespicker.js"></script>
		<script>$j = jQuery.noConflict();</script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<!---date time calender scripts--->
		<link rel="stylesheet" href="images/main.css">
		<script type="text/javascript" language="javascript" src="images/prototype-1.js"></script>
		<script type="text/javascript" language="javascript" src="images/prototype-base-extensions.js"></script>
		<script type="text/javascript" language="javascript" src="images/prototype-date-extensions.js"></script>
		<script type="text/javascript" language="javascript" src="images/behaviour.js"></script>
		<script type="text/javascript" language="javascript" src="images/datepicker.js"></script>
		<link rel="stylesheet" href="images/datepicker.css">
		<script type="text/javascript" language="javascript" src="images/behaviors.js"></script>
<script>
$j(document).ready(function($) {
//alert('ready');

		$('#datePick1').multiDatesPicker();
       $('#create_id').click(function() {
                var count = 0;
					if($('#location').val() == "NULL")
					{
						$('#location').focus();
						$('#regLocErr').html("Please choose location");
						count++;
								
					}
					if(count>0)
					return false;
               if ($('#datePick ').val() == "") 
			   {
                        $('#datePick').focus();
                        $('#regDateErr').html("Date is required");
						count++;
               }
			 if(count>0)
			 return false;
			 if ($('#starttime').val() == "")
			 {
                        $('#starttime').focus();
                        $('#regStartErr').html("Start Time  is required");
						count++;
             }
			 if(count>0)
			return false;
			if ($('#enddatetime').val() == "") 
			{
               $('#address2').focus();
               $('#regEndErr').html("End Time is required");
				count++;
            }
				 

if(count>0)
return false;
var timediff1=$('#enddatetime').val();
var timediff2=$('#starttime').val()

alert(timediff);
if ($('#starttime').val() > $('#enddatetime').val())
{
alert("Please enter the correct time interval of 1 hour");
count++;
}
if(count>0)
return false;
});




});
</script>
</head>

 <style>
 
            #regLocErr
			{
			color:#FE2E2E;
			}
			#regDateErr
			{
			color:#FE2E2E;
			}
			#regStartErr
			{
			color:#FE2E2E;
			}
			#regEndErr
			{
			color:#FE2E2E;
			}
            input[title]:hover:after {
                content: attr(title);
                padding: 4px 8px;
                color: #333;
                position: absolute;
                left: 0;
                top: 100%;
                white-space: nowrap;
                z-index: 20;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
                -moz-box-shadow: 0px 0px 4px #222;
                -webkit-box-shadow: 0px 0px 4px #222;
                box-shadow: 0px 0px 4px #222;
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #eeeeee),color-stop(1, #cccccc));
                background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
            }
            .ui-autocomplete{max-height: 240px;overflow: hidden;font-size: 0.6em;}
            .select_dropdown{-webkit-appearance: none;width: 150px;background: url(assets/newui/dropdown_bg.png) no-repeat;background-size: 25px 24px;background-position: 99% 1px;height: 28px;border: 1px solid #dcdcdc;background-color: white;font-size: 15px;outline: none;font-family: arial;color: #8e8c8c;}
            #popup_wrapper
			{
                color:#000000;
                font-family:tahoma;
                font-size:15px;
                margin:0 auto;
            }
            .popup_content{ position: relative;margin: 5px;border: 1px dotted black; }
            .popup_content_box{margin:15px;}
            .popup_box{
                background: white;
                /*box-shadow: 0 1px 2px;*/
                color: #000000;
                width: 40%;
                left: 0;
                position: fixed;
                right: 0;
                top: 110%;
                z-index: 101;
                border-radius: 2px;
                margin: 0 auto;
                border: 1px solid #5c82e1;
                /*       border-bottom-right-radius: 0px;
               border-bottom-left-radius: 0px;
               -moz-border-radius-bottomleft: 0px;
               -moz-border-radius-bottomright: 0px;
                       -moz-border-radius:10px;*/
                margin:0 auto;
            }

            .popup_overlay{
                background: #ccc;
                bottom: 0;
                left: 0;
                position: fixed;
                right: 0;
                top: 0;
                z-index: 100;
                opacity:0;
                cursor: pointer;
                display: none;
            }

            a.popup_boxclose{
                /*background: url("assets/cancel.png") repeat scroll left top transparent;*/
                cursor: pointer;
                float: right;
                height: 26px;
                position: relative;
                top: 35px;
                width: 26px;
                font-size: 18px;
                text-decoration: none;
            }
            .page_body_content 
            {
                height: 2000px!important;
                background: #ededed;
            }

			body
			{
		
			font-size: 16px;
			}
        </style>

				<div class="modal" id="modal_default_4"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				<div class="modal-content" style=" color: white;background: white;">
				<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <!--<h4 class="modal-title">ADD  the Program Details..</h4>-->
            </div>
            <div class="modal-body clearfix" style="position: relative;">
                <div id="spinner_image" style="display:none;position:absolute;top:40%;left:40%;"><img src="img/spinner.gif" /></div>
                <!--<form id="popup1" action="Report1.php" method="Post">-->
                <table class="table table-bordered table-striped table-hover">
                    <tbody>


                   NEW PASSWORD: <input type="text"   name="pass" style="width: 200px;margin-left: 130px;margin-top: -23px;" id="chng_pass">&nbsp;<span style="color:red;" id="errmsg"></span><br> <br>
                   CONFIRM PASSWORD <input type="text" name="conf_pass" style="width: 200px;margin-left: 130px;
                                            margin-top: -23px;" id="chng_conf_pass" ><br><br>
                    <input type="hidden" name="sendData" id="sendData"/>
                    </tbody>
                </table>        



                <!--</form>-->


                <?php
                if (isset($_REQUEST['title'])) {
                    $delete_admin_id1 = "Update doctor Set password='" . $_REQUEST['title'] . "' where doc_id= '<?php echo $i; ?>";
                    $delete_admin_val = mysql_query($delete_admin_id1);
                }
                ?>
            </div>
            <div class="modal-footer">                
                <button type="button"  style="margin-right: 26%; width: 90px;" class="btn btn-warning btn-clean" data-dismiss="modal" id="close_popup">Cancel</button>
                <button type="button" style="float: left;margin-left: 30%;" name="Add" id="change_pass" class="btn btn-warning btn-clean">Submit form</button>
            </div>
        </div>
    </div>
</div>  
<div class="page-sidebar" style="width: 150px;">
                    <div class="page-navigation-panel" style="display: none">
                    <div class="name"></div>
                    <div class="control"><!--<a href="#" class="psn-control"><span class="icon-reorder"></span></a>--></div>
                </div>            
                <div class="page-navigation-panel search">                               
                    <div class="input-group"> 


                    </div>                
                </div>
                <ul class="page-navigation" style="width: 172px;margin-top: 50px;">
                    <li>
                        <a  href="testprofile.php" class="menu_item" id="home-block" style="font-size: 14px; height: 47px; font-family: helvetica neue light; color: #AAAAAA;" >MY PROFILE
                
                        </a>
   
                  
                        <a  href="getinvoice.php" class="menu_item" id="home-block" style="font-size: 14px; height: 47px; font-family: helvetica neue light; color: #AAAAAA;" >INVOICE

                            
                        </a>


                        <a  href="createschedule.php" class="menu_item" id="home-block" style="font-size: 14px; height: 47px; font-family: helvetica neue light; color: #AAAAAA;" >CREATE SCHEDULE

                            
                        </a>
						 <a  href="createlocation.php" class="menu_item" id="home-block" style="font-size: 14px; height: 47px; font-family: helvetica neue light; color: #AAAAAA;" >ADD LOCATION</a>
						 
                   


                </ul>

            </div>
            
<div class="content">
<div style="margin-left: auto;margin-right: auto;width: 400px;margin-top:115px;">
<div style="color: #fff;font-size: 25px;margin-bottom: 48px;">CREATE BOOKING SLOT </div>
<form action="" id="schedule_form">

<div>
<label style="font-size: 16px;color:#fff;">SELECT A LOCATION</label>
<select name="location"  id="location" style="cursor: pointer;-webkit-appearance: none;width:300px;">
<option value="NULL" style="font-size: 16px;">Choose Location</option>
<?php
foreach ($cursor as $val) 
{?>
  
    <option value="<?php echo $val['_id'];?>" id="<?php echo $val['user'];?>" ><?php echo $val['locName'];?></option>

<?php
}
?>
</select>
<div id="regLocErr"></div>
</div>
<br/>
<div>
<label style="font-size: 16px;color:#fff;">SELECT DATE FOR SLOT</label>
<div class="box">
<input type="text" id="datePick1" name="dates" class="" style="color:#000;width:300px;">
</div>
<div id="regDateErr"></div>
</div>
<div>
<label style="font-size: 16px;color:#fff;">START  TIME:</label>
<div>
<input type="text" name="starttime" class="timepicker"  id="starttime" style="color:#000;width:300px;"/>
</div>
<div id="regStartErr"></div>
</div>

<div>
<label style="font-size: 16px;color:#fff;">END  TIME:</label>
<div>
<input type="text" name="enddatetime" class="timepicker" id="enddatetime" style="color:#000;width:300px;" />
</div>
<div id="regEndErr"></div>
</div>
<br/>
<div style="margin-left: 40px;">
<input type="submit" name="create" value="CREATE" id="create_id" style="width:100px;float:left;"/>
</div>
<div>
<input type="submit" name="cancel" value="CANCEL" style="width:100px;float:left;margin-left:20px;"/>
</div>
</form>
</div>
</div>

<script>
    $(document).ready(function() {
        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });
        $('#change_pass').click(function() {

            var pass = $('#chng_pass').val();
            var conf_pass = $('#chng_conf_pass').val();
            if (pass == '' || conf_pass == '') {
                alert('Passwords are mandatory.');
            } else if (pass != conf_pass) {
                alert('Passwords does not match, check once.');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {doc_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        if (result.flag == 0) {


                            $('#chng_pass').val("");
                            $('#chng_conf_pass').val("");

                            $('#close_popup').trigger('click');
                            $('.modal-backdrop.in').css('display', 'none');


                        }
                    }
                });
            }
        });


    });
</script>

<script>
    $(document).ready(function() {
//called when key is pressed in textbox
        $("#chng_pass").blur(function() {
            //if the letter is not digit then display error and don't type anything

            var zipcode_expression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
            if (zipcode_expression.test($.trim($('#chng_pass').val())) == false) {
// $("#errmsg").html("invalid zipcode").show().fadeOut("slow")
                alert('Password must contain atleast one digit,one Uppercase, one Lower case character and atleast 8 digit ');
            }


            // $("#errmsg").html("Digits Only").show().fadeOut("slow");


        });
    });
</script>

<script>
$(document).ready(function() {
 $('#popup_overlay').click(function(){
   closeDetailDialog('popup_boxpopup');
  });
$('#showpopup').click(function() {
//$('#offers_data_div').load('get_offers.php', {specialoffers: 'yes', total_price: $('#total_price').val()}, function() {
$('#popup').css('display', 'block');
});
$('#get_latlong').click(function(){
                    var addr1 = $('.address1').val();
                    var addr2 = $('.address2').val();
                    //var country = $(".country option:selected").text();
					//var suburb=$(".Suburb").val();
                    //var city = $(".state option:selected").text();
                    //var postcode = $('.postcode').val();
                    var flag = 0;
                    if (addr1 == '' || addr2 == '') {
                        $('#latlong_error').text('Enter complete address to get latlong');
                        flag++;
                    }
		            if (flag > 0)
                    return false;
		            var addr = addr1 + ' ' + addr2 ;
					alert(addr);
                    $('#map_lat_long').load('http://privemd.com/admin/get_latlong.php', {address: addr}, function() {
                        var latlong = $('#map_lat_long').text().split(">");
						alert(latlong);
                        var lat = latlong[1].split('[');
                        var longs = latlong[2].split(')');
                        $('.latitude').val(lat[0]);
                        $('.longitude').val(longs[0]);
                    });
                });
                 $('#show_map_addr').click(function() {
                    var addr = $('#addr_for_map_here').val();
                    $('#map_lat_long').load('http://privemd.com/admin/get_latlong.php', {address: addr}, function() {
                        var latlong = $('#map_lat_long').text().split(">");
                        var lat = latlong[1].split('[');
                        var long = latlong[2].split(')');
                        var myLatlng = new google.maps.LatLng(eval(lat[0]), eval(long[0]));
                        var myOptions = {
                            zoom: 10,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            disableDefaultUI: true,
                            streetViewControl: true,
                            zoomControl: true,
                            scaleControl: true
                        };
                        map = new google.maps.Map(document.getElementById("plot_map_here"), myOptions);
                        var marker = new google.maps.Marker({
                            position: myLatlng
                        });
                        marker.setMap(map);
                        google.maps.event.addListener(map, 'click', function(event)
                        {
                            //alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() ); 
                            $('#menuse_latitude').attr('value', event.latLng.lat());
                            $('#menuse_longitude').attr('value', event.latLng.lng());
                            closeDetailDialog('popup_boxpopup');
                        });

                        google.maps.event.addListener(marker, "click", function(event)
                        {
                            $('#menuse_latitude').attr('value', event.latLng.lat());
                            $('#menuse_longitude').attr('value', event.latLng.lng());
                            closeDetailDialog('popup_boxpopup');
                        });
                        var infowindow = new google.maps.InfoWindow();
                        google.maps.event.addDomListener(window, 'click', function() {
                            infowindow.setContent("Current position: latitude: " + lat + " longitude: " + lon);
                            infowindow.open(map, currentPositionMarker);
                        });
                    });
                });
            });
            var map;
            var marker = null;

            function plot_map(lat, long) {
                var myLatlng = new google.maps.LatLng(lat, long);
                var myOptions = {
                    zoom: 10,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    draggable: false,
                    disableDefaultUI: true,
                    streetViewControl: false,
                    zoomControl: false,
                    scaleControl: false
                };
                map = new google.maps.Map(document.getElementById("plot_map_here"), myOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng
                });

                marker.setMap(map);
                myCity.setMap(map);
            }

            google.maps.event.addDomListener(window, 'click', plot_map);

			
				
			function openDetailDialog() 
			{
				    if ($('.address1').val() !== '') {
                    var address = $('.address1').val() +','+ $('.address2').val();
                    $('#addr_for_map_here').val(address);
                    $('#show_map_addr').trigger('click');
                } else {
                    
                }
                $('#popup_boxpopup').css('display', 'block');
                $('html, body').animate({scrollTop: '0px'}, 'slow');
                $('#popup_overlay').addClass('popup_overlay');
                $('.popup_overlay').css('opacity', '0.8');
                $('.popup_overlay').fadeIn('fast', function() {
                    $('#popup_boxpopup').css('display', 'block');
                    $('#popup_boxpopup').animate({'top': '5%'});
                    $('#popup_boxpopup').css('position', 'absolute');
                });
            }
          function closeDetailDialog(prospectElementID) {
                $('#popup_boxpopup').css('display', 'none');
                $('#' + prospectElementID).css('position', 'absolute');
                $('#' + prospectElementID).animate({'tp': '-100%'}, 1, function() {
                    $('#' + prospectElementID).css('position', 'fixed');
                    $('#' + prospectElementID).css('top', '110%');
                    $('#popup_overlay').hide();
                });
            }
			


				
</script>
<script>
    $(document).ready(function() {
//called when key is pressed in textbox
        $("#close_popup").click(function() {
            $('#chng_pass').val("");
            $('#chng_conf_pass').val("");

            // $('#close_popup').trigger('click');
            $('.modal-backdrop.in').css('display', 'none');


        });
    });
</script>

		
		
