<?php
session_start();
include('../../Models/ConDB.php');
$db = new ConDB();
if (isset($_REQUEST['submit']))
{
    $checkUser = "SELECT mas_id,first_name,profile_pic,email,password FROM master WHERE Status=3 and email= '" . $_REQUEST['username'] . "' and password= '" . $_REQUEST['password'] . "'";
  
    $ifUserAvailable = mysql_query($checkUser, $db->conn);
    $getcount = mysql_num_rows($ifUserAvailable);
    if ($getcount > 0) {
        $rows = mysql_fetch_assoc($ifUserAvailable);
        $_SESSION['admin1'] = $rows['first_name'];
        $_SESSION['admin_ids'] = $rows['mas_id'];
        $_SESSION['admin_name'] = $rows['first_name'];
        $_SESSION['admin_pic'] = $rows['profile_pic'];
        $oneHourExp = (24 * 60) + time();
   $_SESSION['validity'] = $oneHourExp;
   header("location:driverprofile.php");
    } else {
        echo "Your Login Name or Password is invalid";
    }
}
$curTime = time();
if (isset($_SESSION['admin_ids']) && $_SESSION['validity'] >= $curTime && isset($_GET['mas_id'])) {
    $_SESSION['admin_ids']=$_GET['mas_id'];
    header('location: driverprofile.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title> Driver Login Page</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link rel="icon" type="image/ico" href="favicon.ico"/>

        <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />        

        <script type='text/javascript' src='js/plugins/jquery/jquery.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script>   
        <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
        <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>

        <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>

        <script type='text/javascript' src='js/plugins.js'></script>    
        <script type='text/javascript' src='js/actions.js'></script>    
        <script type='text/javascript' src='js/settings.js'></script>
    </head>
    <body class="bg-img-num1"> 

        <div class="container" style="background: rgb(42, 42, 42);">        

            <div class="login-block">
                <div class="block block-transparent">
                    <div style="text-align: center;color:white;"><h2>Driver Login</h2></div>
                    <div class="head">
                        <div style="margin-left: auto;margin-right: auto;width: 88px;"><img src="../img/user.jpg" class="img-circle img-thumbnail"></div>                        
                        </div>
                    </div>
                    <form action="" method="post">
                        <div class="content controls npt">
                            <div class="form-row user-change-rows">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="icon-user"></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Login" name="username"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="icon-key"></span>
                                        </div>
                                        <input type="password" class="form-control" placeholder="Password" name="password"/>
                                    </div>
                                </div>
                            </div>                        
                            <div class="form-row">
                                <div class="col-md-6">
                                    <!-- <a href="#" class="btn btn-default btn-block btn-clean">Register</a>  -->                             
                                </div>
                                <div class="col-md-6">
                                    <a href="#"    class="btn btn-default btn-block btn-clean"><input type="submit" value="Log In" name="submit"/></a>
                                </div>
                            </div>

                            
							
                                <div class="form-row">
                                    <div class="col-md-12">
                                         <!--  <a href="#" class="btn btn-info btn-block"><span class="icon-facebook"></span> &nbsp; Facebook</a>-->  
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <!--  <a href="#" class="btn btn-primary btn-block"><span class="icon-twitter"></span> &nbsp; Tweeter</a>-->  
                                    </div>
                                </div>                    
                            </div>

                        </div>
                    </form>
                </div>

            </div>

    </body>
</html>
