<?php

error_reporting(0);
include('../Models/ConDB.php');
include('../Models/class.phpmailer.php');
include('../Models/mandrill/src/Mandrill.php');
include('../Models/sendAMail.php');

$db1 = new ConDB();
$firstname = $_REQUEST['firstname'];
$lastname = $_REQUEST['lastname'];
$password = $_REQUEST['password'];
$username = $_REQUEST['username'];
$address = $_REQUEST['address'];
$email = $_REQUEST['email'];
$mobile = $_REQUEST['mobile'];
//$postcode = $_REQUEST['zipcode'];
$expirationdt = $_REQUEST['expirationrc'];
//$vehicleid = $_REQUEST['companyid'];

$checkUserQry = "select mas_id from master where email = '" . $email . "'";
if (mysql_num_rows(mysql_query($checkUserQry, $db1->conn)) > 0) {

    echo json_encode(array('msg' => "email already exist, choose another", 'flag' => 1));
    return false;
}

$checkUserQry1 = "select mas_id from master where mobile = '" . $mobile . "'";
if (mysql_num_rows(mysql_query($checkUserQry1, $db1->conn)) > 0) {

    echo json_encode(array('msg' => "Mobile number already exist, choose another", 'flag' => 1));
    return false;
}

if ($_FILES['certificate']['size'] > 1000000 || $_FILES['insurcertificate']['size'] > 1000000 || $_FILES['carriagecertificate']['size'] > 1000000) {
    echo json_encode(array('msg' => "File size cannot be more than 1 MB", 'flag' => 1));
    return false;
}

if (
        !isset($_FILES['certificate']['error']) || is_array($_FILES['certificate']['error'])
) {
    echo json_encode(array('msg' => $_FILES['certificate']['error'], 'flag' => 1, 't' => $_FILES['certificate']['size']));
    return false;
}
//certificate of registration(name sampling)	
$name = $_FILES["certificate"]["name"];
$ext = end((explode(".", $name))); # extra () to prevent notice
$cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
//insurance certificate
$passbooks = $_FILES["passbook"]["name"];
$ext1 = end((explode(".", $passbooks)));
$passbook = (rand(1000, 9999) * time()) . '.' . $ext1;
//photos
$photos = $_FILES["photos"]["name"];
$ext2 = strtolower(end((explode(".", $photos))));
$photosnew = (rand(1000, 9999) * time()) . '.' . $ext2;

$valid_exts = array("jpg", "jpeg", "gif", "png");

if (!in_array($ext2, $valid_exts)) {
    echo json_encode(array('msg' => "Please upload any of jpeg,png or gif only", 'flag' => 1));
    return false;
}

if (!move_uploaded_file($_FILES['certificate']['tmp_name'], sprintf('../pics/%s', $cert_name))) {
    echo json_encode(array('msg' => "Failed to upload certificate-1", 'flag' => 1));
    return false;
}

if (!move_uploaded_file($_FILES['passbook']['tmp_name'], sprintf('../pics/%s', $passbook))) {
    echo json_encode(array('msg' => "Failed to upload certificate-2", 'flag' => 1));
    return false;
}

$newwidth = "54";
$newheight = "55";

if (!move_uploaded_file($_FILES['photos']['tmp_name'], sprintf('../pics/%s', $photosnew))) {
//    exec('convert ' . $photosnew . ' -resize ' . $newwidth . 'x' . $newheight . '^ ' . $thumbnail);
    //rename($upload_image, $actual);
    echo json_encode(array('msg' => "Failed to upload photo-3", 'flag' => 1));
    return false;
}

$file_to_open = "../pics/" . $photosnew;

list($width, $height) = getimagesize($file_to_open);

if ($width > $height) {

    $newwidth = $width;
    $newheight = ($newwidth / $width) * $height;
} else {

    $newheight = $height;
    $newwidth = ($newheight / $height) * $width;
}


$ratio = $newheight / $newwidth;

/* mdpi 36*36 */
$mdpi_nw = 36;
$mdpi_nh = $ratio * 36;

$mtmp = imagecreatetruecolor($mdpi_nw, $mdpi_nh);

if ($ext2 == "jpg" || $ext2 == "jpeg") {
    $image = imagecreatefromjpeg($file_to_open);
} else if ($ext2 == "gif") {
    $image = imagecreatefromgif($file_to_open);
} else if ($ext2 == "png") {
    $image = imagecreatefrompng($file_to_open);
}

//$mdpi_image = imagecreatefromjpeg($file_to_open);

imagecopyresampled($mtmp, $image, 0, 0, 0, 0, $mdpi_nw, $mdpi_nh, $width, $height);

$mdpi_file = '../pics/mdpi/' . $photosnew;


if (imagejpeg($mtmp, $mdpi_file, 100)) {
//    echo "mdpi success";
} else {
//    echo "mdpi failed";
}

/* HDPI Image creation 55*55 */
$hdpi_nw = 55;
$hdpi_nh = $ratio * 55;

$tmp = imagecreatetruecolor($hdpi_nw, $hdpi_nh);

//$hdpi_image = imagecreatefromjpeg($file_to_open);

imagecopyresampled($tmp, $image, 0, 0, 0, 0, $hdpi_nw, $hdpi_nh, $width, $height);

$hdpi_file = '../pics/hdpi/' . $photosnew;

imagejpeg($tmp, $hdpi_file, 100);

/* XHDPI 84*84 */
$xhdpi_nw = 84;
$xhdpi_nh = $ratio * 84;

$xtmp = imagecreatetruecolor($xhdpi_nw, $xhdpi_nh);

//$xhdpi_image = imagecreatefromjpeg($file_to_open);

imagecopyresampled($xtmp, $image, 0, 0, 0, 0, $xhdpi_nw, $xhdpi_nh, $width, $height);

$xhdpi_file = '../pics/xhdpi/' . $photosnew;

imagejpeg($xtmp, $xhdpi_file, 100);

/* xXHDPI 125*125 */
$xxhdpi_nw = 125;
$xxhdpi_nh = $ratio * 125;

$xxtmp = imagecreatetruecolor($xxhdpi_nw, $xxhdpi_nh);

//$xxhdpi_image = imagecreatefromjpeg($file_to_open);

imagecopyresampled($xxtmp, $image, 0, 0, 0, 0, $xxhdpi_nw, $xxhdpi_nh, $width, $height);

$xxhdpi_file = '../pics/xxhdpi/' . $photosnew;

imagejpeg($xxtmp, $xxhdpi_file, 100);


$insert_vechile = "INSERT INTO `master`(`first_name`, `last_name`, `email`, `mobile`,`password`, `Status`,`created_dt`,`profile_pic`) VALUES ('" . $firstname . "','" . $lastname . "','" . $email . "','" . $mobile . "',md5('" . $password . "'),'1',now(),'" . $photosnew . "')";
$insert_vechile_res = mysql_query($insert_vechile, $db1->conn);
$get_last_inserted_id = mysql_insert_id();
//$get_vehicle_type="select * from 


if ($get_last_inserted_id <= 0) {

    echo json_encode(array('msg' => "Failed to add driver", 'flag' => 1, 'q' => $insert_vechile));
    return false;
} else {
    $mongo = $db1->mongo;
    $location = $mongo->selectCollection('location');
    $curr_date = time();
    $curr_gmt_dates = gmdate('Y-m-d H:i:s', $curr_date);
    $curr_gmt_date = new MongoDate(strtotime($curr_gmt_dates));
    $mongoArr = array("type" => 0, "user" => (int) $get_last_inserted_id, "name" => $firstname, "lname" => $lastname,
        "location" => array(
            "longitude" => 0,
            "latitude" => 0
        ), "image" => $photosnew, "rating" => 0, 'status' => 1, 'email' => strtolower($email), 'dt' => $curr_gmt_date->sec
    );

    $location->insert($mongoArr);

    $mail = new sendAMail($db1->host);
    $err = $mail->sendMasWelcomeMail(strtolower($email), ucwords($firstname));
}

/* if(mysql_insert_id() > 0)
  echo json_encode(array('qry'=>$insert_vechile,'flag' => 0));
  else
  echo json_encode(array('qry'=>$insert_vechile,'flag' => 1)); */

$insert_doc = "INSERT INTO docdetail(driverid, url,expirydate,doctype) VALUES ('" . $get_last_inserted_id . "','" . $cert_name . "','" . $expirationdt . "','1'),('" . $get_last_inserted_id . "','" . $passbook . "','00-00-00','2')";
$insert_doc_res = mysql_query($insert_doc, $db1->conn);
echo json_encode(array('msg' => "Driver added", 'flag' => 0));
?>