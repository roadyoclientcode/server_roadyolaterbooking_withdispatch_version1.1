<?php

session_start();
//require '../class/ConDB.php';
//echo $_FILES["agent_profile_pic"]['name'];
//return FALSE;
$extra_arg = '';
//echo $_REQUEST['photo_number'];
if (isset($_FILES["agent_profile_pic"]['name']) && $_FILES["agent_profile_pic"]["name"] != '') {
    $picinfo = @getimagesize($_FILES["agent_profile_pic"]['tmp_name']);
    if ($picinfo !== false) {
        $orgnl_width = $picinfo [0];
        $orgnl_height = $picinfo [1];
    }
    if ($orgnl_width > 600) {
        $orgnl_width = 600;
    }
    if ($orgnl_height > 400) {
        $orgnl_height = 400;
    }
    $images_dir = "../pics/";
    $image_type = 'agent_profile_pic';
    $upload_and_resize_orgnl = generate_resized_image($_FILES["agent_profile_pic"], $orgnl_width, $orgnl_height, $images_dir, $_SESSION['user_id'], $image_type, $_REQUEST['photo_number']);
    echo $upload_and_resize_orgnl;
}

function generate_resized_image($file, $image_width, $image_height, $dir, $new_id, $image_type, $extra) {
    $max_dimension = 50000; // Max new width or height, can not exceed this value.
    // $dir = "./images/";             Directory to save resized image. (Include a trailing slash - /)
// Collect the post variables.
    $postvars = array(
        "image" => str_replace('"', "-", str_replace("'", "-", trim($file["name"]))),
        "image_tmp" => $file["tmp_name"],
        "image_size" => (int) $file["size"],
        "image_max_width" => 302,
        "image_max_height" => 140
    );
// Array of valid extensions.
    $valid_exts = array("jpg", "jpeg", "gif", "png");
// Select the extension from the file.
    $ext = end(explode(".", strtolower(trim($file["name"]))));
// Check not larger than 1MB.
    if ($postvars["image_size"]) {
// Check is valid extension.
        if (in_array($ext, $valid_exts)) {
            if ($ext == "jpg" || $ext == "jpeg") {
                $image = imagecreatefromjpeg($postvars["image_tmp"]);
            } else if ($ext == "gif") {
                $image = imagecreatefromgif($postvars["image_tmp"]);
            } else if ($ext == "png") {
                $image = imagecreatefrompng($postvars["image_tmp"]);
            }
// Grab the width and height of the image.
            list($width, $height) = getimagesize($postvars["image_tmp"]);
// If the max width input is greater than max height we base the new image off of that, otherwise we
// use the max height input.
// We get the other dimension by multiplying the quotient of the new width or height divided by
// the old width or height.
            if ($postvars["image_max_width"] > $postvars["image_max_height"]) {
                if ($postvars["image_max_width"] > $max_dimension) {
                    $newwidth = $max_dimension;
                } else {
                    $newwidth = $postvars["image_max_width"];
                }
                $newheight = ($newwidth / $width) * $height;
            } else {
                if ($postvars["image_max_height"] > $max_dimension) {
                    $newheight = $max_dimension;
                } else {
                    $newheight = $postvars["image_max_height"];
                }
                $newwidth = ($newheight / $height) * $width;
            }
// Create temporary image file.
            $tmp = imagecreatetruecolor($newwidth, $newheight);
// Copy the image to one with the new width and height.
            imagecopyresampled($tmp, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
// Create random 4 digit number for filename.
            $rand = rand(1000, 9999);
            $filename = $dir . $new_id . $postvars["image"];
            $filename_db = $new_id . $postvars["image"];


            $pic_width = 202;
            $pic_height = 202;
            $class_name = "class='click_to_crop'";
            $image_title = "Your profile picture";
            //$ret = update_agent_profile_pic($filename_db, $new_id);

            $image_url = '' . $filename;
// Create image file with 100% quality.
            imagejpeg($tmp, $filename, 100);
            $photo_number = '';

            if ($extra != '0')
                $photo_number = $extra;


            return "
<img $class_name src=\"" . $filename . "\" border=\"0\" title=\"$image_title\" width=\"" . $pic_width . "\" height=\"" . $pic_height . "\" photo_number='" . $photo_number . "'/><input type='hidden' name='advertiser_logo_image" . $photo_number . "' value='" . $image_url . "' /><script>$('.click_to_crop').trigger('click');</script>";

            imagedestroy($image);
            imagedestroy($tmp);
        } else {
            return "Invalid file type. You must upload an image file. (jpg, jpeg, gif, png).";
        }
    } else {
        return "File not available, choose another.";
    }
}

function insert_filename($filename, $new_id) {
//          $db_1 = new ConDB();
//          $qry = "update property set thumbnail='" . $filename . "' where property_id=" . $new_id;
//          mysql_query($qry,$db_1->conn);
}

function update_in_thumb($filename, $new_id) {
//          $db_5 = new ConDB();
//          $qry = "update propertyimages set name = '".$filename."' where property_id = ".$new_id." and flag='main'" ;
//          mysql_query($qry,$db_5->conn);
}

function insert_thumb($filename, $new_id) {
//          $db_2 = new ConDB();
//          $qry = "insert into propertyimages(name,property_id) values('" . $filename . "','" . $new_id . "')";
//          mysql_query($qry,$db_2->conn);
}

function update_agent_profile_pic($filename, $new_id) {
//          $db_3 = new ConDB();
//          $qry = "update agents set photo='" . $filename . "' where agent_id=" . $new_id;
//          mysql_query($qry,$db_3->conn);
//          return $qry;
}

function update_agency_logo($filename, $new_id) {
//          $db_4 = new ConDB();
//          $qry = "update agency set logo='" . $filename . "' where agency_id=" . $new_id;
//          mysql_query($qry,$db_4->conn);
}

?>