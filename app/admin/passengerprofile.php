<?php
session_start();
error_reporting(1);
include('../Models/ConDB.php');
$db = new ConDB();
if (isset($_GET['slave_id'])) {
    $_SESSION['admin_idsnew'] = $_GET['slave_id'];
}

$disable = '';
$curTime = time();

if (!isset($_SESSION['admin_idsnew']) || $_SESSION['validity'] <= $curTime) {
    header('location: logoutpassenger.php');
}
if ($_SESSION['admin'] != 'super') {
    $disable = 'disabled';
}
if ($_SESSION['admin'] != 'super' && $_SESSION['admin_idsnew'] == '') {

    header('location: SuperAdminLogin.php');
}
$oneHourExp = (24 * 60) + time();
$_SESSION['validity'] = $oneHourExp;
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title>PASSENGER ADMIN</title>

        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />        
        <script type='text/javascript' src='js/plugins/jquery/jquery.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script>   
        <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
        <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='js/actions.js'></script>
        <script type='text/javascript' src='js/settings.js'></script>
        <script type='text/javascript' src='js/plugins.js'></script>

        <link rel='stylesheet' type='text/css' href='menu_source/styles.css' />
        <script type='text/javascript' src='menu_source/menu_jquery.js'></script>

        <style>
            #overlay_div{display: none;}
            .container{max-width: none !important;background: #fff;color: #000;}
            .page-content{background:#fff;}
            .page-container{box-shadow:none;}



        </style>

        <script>
            $(document).ready(function() {
                window.type = '1';

                $('#overlay_div').toggle();
                $('.container').load('profilepass.php', {type: '1'}, function() {
                    $('#overlay_div').toggle();
                });

                $('.list_item').click(function() {
                    var dis = $(this);

                    if (dis.attr('data') != window.type)
                    {
                        $('#overlay_div').toggle();
                        window.type = dis.attr('data');
                        $('.container').load('profilepass.php', {type: dis.attr('type')}, function() {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item2').click(function() {
                    var dis = $(this);

                    if (dis.attr('data') != window.type) {
                        $('#overlay_div').toggle();
                        window.type = dis.attr('data');
                        $('.container').load('getreviewforcompany.php', {type: dis.attr('type')}, function() {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item3').click(function() {
                    var dis = $(this);

                    if (dis.attr('data') != window.type) {
                        $('#overlay_div').toggle();
                        window.type = dis.attr('data');
                        $('.container').load('bookingpassenger.php', {type: dis.attr('type')}, function() {
                            $('#overlay_div').toggle();
                        });
                    }

                });



                $('#change_pass_admin').click(function() {

                    var pass = $('#chng_pass').val();
                    var conf_pass = $('#chng_conf_pass').val();
                    if (pass == '' || conf_pass == '') {
                        alert('Passwords are mandatory.');
                    } else if (pass != conf_pass) {
                        alert('Passwords does not match, check once.');
                    } else if (checkStrength(pass) == 1) {
                        alert('Password must contain atleast one digit,one Uppercase, one Lower case character and atleast 8 digit ');
                    } else {

                        $.ajax({
                            type: "POST",
                            url: "changePassPassenger.php",
                            data: {doc_id: $('#sendData_a').val(), pass: conf_pass, type: 1},
                            dataType: "JSON",
                            success: function(result) {
                                alert(result.message);
                                if (result.flag == 0) {

                                    $('#chng_pass').val("");
                                    $('#chng_conf_pass').val("");

                                    $('#change_pass_cancel').trigger('click');

                                }
                            }
                        });
                    }
                });

            });

            function checkStrength(password)
            {
                //initial strength
                var strength = 0;

                //if the password length is less than 6, return message.
                if (password.length < 6) {
                    return 1;
                }

                //length is ok, lets continue.

                //if length is 8 characters or more, increase strength value
                if (password.length > 7)
                    strength += 1;

                //if password contains both lower and uppercase characters, increase strength value
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                    strength += 1;

                //if it has numbers and characters, increase strength value
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                    strength += 1;

                //if it has one special character, increase strength value
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                    strength += 1;

                //if it has two special characters, increase strength value
                if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                    strength += 1;

                //now we have calculated strength value, we can return messages

                //if value is less than 2
                if (strength < 3)
                {
                    return 2;
                }
            }

            function hover(element)
            {
                element.setAttribute('src', 'images/driver_on.png');
                element.setAttribute('color', '#fff');
            }

            function unhover(element)
            {
                element.setAttribute('src', 'images/driver_off.png');
            }
            function passengerhover(element)
            {
                element.setAttribute('src', 'images/passenger_on.png');
            }
            function passengerunhover(element)
            {
                element.setAttribute('src', 'images/passenger_off.png');
            }
            function reviewhover(element)
            {
                element.setAttribute('src', 'images/ratings_on.png');
            }
            function reviewunhover(element)
            {
                element.setAttribute('src', 'images/ratings_off.png');
            }
            function bookhover(element)
            {
                element.setAttribute('src', 'images/calendar_on.png');
            }
            function bookunhover(element)
            {
                element.setAttribute('src', 'images/calendar_off.png');
            }
            function financehover(element)
            {
                element.setAttribute('src', 'images/finance_on.png');
            }
            function financeunhover(element)
            {
                element.setAttribute('src', 'images/finance_off.png');
            }
            function companyhover(element)
            {
                element.setAttribute('src', 'images/company_on.png');
            }
            function companyunhover(element)
            {
                element.setAttribute('src', 'images/company_off.png');
            }
            function bookhover(element)
            {
                element.setAttribute('src', 'images/book_on.png');
            }
            function bookunhover(element)
            {
                element.setAttribute('src', 'images/book_off.png');
            }
            function roadyohover(element)
            {
                element.setAttribute('src', 'images/roadyo_icon_on.png');
            }
            function roadyounhover(element)
            {
                element.setAttribute('src', 'images/roadyo_icon_off.png');
            }


        </script>

    </head>
    <body class="bg-img-num1"> 
        <div class="header">
            <div class='root'>
                <div style="width: 100%; height:55px;text-align: center; background-image: url(images/header_bar.png);">

                    <div class="modal modal-draggable" id="modal_default_13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content" style="color:#000;">                 
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Change passenger admin password</h4>
                                </div>                
                                <div class="modal-body clearfix" style="text-align:center;">
                                    <label style="width:150px;">New Password:</label> <input type="password"   name="pass" style="width: 200px;display: inline;" id="chng_pass"><br><br>
                                    <label style="width:150px;">Confirm Password:</label> <input type="password" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass" ><br>
                                    <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_idsnew']; ?>"/>
                                    <span style="color:red;" id="errmsg"></span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning btn-clean" id="change_pass_admin">Submit</button>              
                                    <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel">Cancel</button>              
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="width: 230px;height: 59px;float: left;background: #00b6ad;" >
                        <img src="images/roadyo_logo.png" />
                    </div>
                    <div style=" background: url(images/logo.png) no-repeat; background-size: 109px 18px; background-position: center 35%; height: 59px;background: #2a2a2a;">

                        <div class="top-bar-right" style="float: right;margin-right: 4%;">
                            <ul class="buttons" style="margin: 0%">

                                <li class="btn-group" style="padding-top: 13px;font-size: 15px;font-weight: bold;">                
                                    <span class="avatar">
                                        <?php
                                        $get_pic = "select profile_pic from slave where slave_id='" . $_SESSION['admin_idsnew'] . "'";
                                        $get_pic_row = mysql_query($get_pic, $db->conn);
                                        $get_pic_fetch = mysql_fetch_assoc($get_pic_row);
                                        ?>
                                        <?php
                                        if ($get_pic_fetch['profile_pic'] == '' || $get_pic_fetch['profile_pic'] == 'NULL') {
                                            ?>
                                            <img src="images/avatar.png" class="img-circle" width="32" height="32" alt="">
                                            <?php
                                        } else {
                                            ?>
                                            <img src="../pics/<?php echo $get_pic_fetch['profile_pic']; ?>" class="img-circle" width="32" height="32" alt="">
                                            <?php }
                                        ?>
                                    </span>
                                    <a href="#" class="dropdown-toggle tip"  title="Dropdown" data-toggle="dropdown" style="color: #888888;">

                                        <?Php echo "" . $_SESSION['admin_name']; ?>
                                    </a>
                                    <ul class="dropdown-menu" role="menu" style="background:#fff;margin-top:18px;">
                                        <li><a href="#modal_default_13" data-toggle="modal" class="btn btn-success btn-block">CHANGE PASSWORD</a></li>

                                        <li><a href="logoutpassenger.php" id="logout" name="logout">LOG OUT</a></li>  
                                    </ul>                                                                            
                                </li>
                            </ul>
                            </a>

                        </div>
                        &nbsp;</div>

                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div style="position: fixed;z-index: 9999;margin: 0;width: 100%;height: 100%;background: #000000;opacity: 0.4;" id="overlay_div">
            <div style="position: relative;top:30%;left:45%;">
                <img src="img/spinner.gif" height="50" width="50">
            </div>
        </div>
        <div class="page-container">
            <div class="page-sidebar">
                <div id='cssmenu'>
                    <ul>
                        <li class='active' style='width:230px;'><a href='driverlogin.php' style="background: none;"><span style="visibility: hidden;">Home</span></a></li>

                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;padding-left: 30px;"><span><img src='images/driver_off.png' onmouseover="hover(this);" onmouseout="unhover(this);" height='14' width='14'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">PASSENGER</span></a>
                            <ul>
                                <li><a href='#' class="list_item" type="1" data="1"><span style="padding-left: 50px;">PASSENGER PROFILE</span></a></li>

                            </ul>
                        </li>
                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;padding-left: 30px;"><span><img src='images/calendar_off.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='14' width='14'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">BOOKING</span></a>
                            <ul>
                                <li><a href='#'class="list_item3" type="7,8" data="35"><span style="padding-left: 50px;">BOOKING HISTORY</span></a></li>


                            </ul>
                        </li>








                    </ul>
                </div>
            </div>
            <div class="page-content">
                <div class="container">        
                </div>
            </div>
        </div>


    </body>
</html>