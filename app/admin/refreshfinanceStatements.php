<?php
error_reporting(0);

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require ('../Models/ConDB.php');
$dbHistory = new ConDB();
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}

function week_start_end_by_date($date, $format = 'Y-m-d') {

    //Is $date timestamp or date?
    if (is_numeric($date) AND strlen($date) == 10) {
        $time = $date;
    } else {
        $time = strtotime($date);
    }

    $week['week'] = date('W', $time);
    $week['year'] = date('o', $time);
    $week['year_week'] = date('oW', $time);
    $first_day_of_week_timestamp = strtotime($week['year'] . "W" . str_pad($week['week'], 2, "0", STR_PAD_LEFT));
    $week['first_day_of_week'] = date($format, $first_day_of_week_timestamp);
    $week['first_day_of_week_timestamp'] = $first_day_of_week_timestamp;
    $last_day_of_week_timestamp = strtotime($week['first_day_of_week'] . " +6 days");
    $week['last_day_of_week'] = date($format, $last_day_of_week_timestamp);
    $week['last_day_of_week_timestamp'] = $last_day_of_week_timestamp;

    return $week;
}

$currTime = time();
$today = date('Y-m-d', $currTime);
if (!isset($_REQUEST['from_d'])) {
    $_REQUEST['from_d'] = $today;
}
if (!isset($_REQUEST['to_d'])) {
    $_REQUEST['to_d'] = $today;
}




if ($_REQUEST['type'] == '1' && $_REQUEST['cond'] == '1') {
    $cond = " DATE(ap.appointment_dt) = '" . $today . "' ";
} else if ($_REQUEST['type'] == '1' && $_REQUEST['cond'] == '2') {
//    $currWeek = date('W', $currTime);
    $weekArr = week_start_end_by_date($currTime);
    $cond = " DATE(ap.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "' ";
} else if ($_REQUEST['type'] == '1' && $_REQUEST['cond'] == '3') {
    $cond = " DATE(ap.appointment_dt) >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY
AND DATE(ap.appointment_dt) < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY ";
} else if ($_REQUEST['type'] == '1' && $_REQUEST['cond'] == '4') {
    $currMonth = date('n', $currTime);
    $cond = " MONTH(ap.appointment_dt) = '" . $currMonth . "' ";
} else if ($_REQUEST['type'] == '1' && $_REQUEST['cond'] == '5') {
    $currMonth = date('n', $currTime);
    $prevMonth = $currMonth - 1;
    if ($prevMonth == 0)
        $prevMonth = 12;

    $cond = " MONTH(ap.appointment_dt) = '" . $prevMonth . "' ";
} else if ($_REQUEST['type'] == '1' && $_REQUEST['cond'] == '6') {
    $bfr3Month = date('Y-m-d', strtotime("-3 months"));
    $cond = " DATE(ap.appointment_dt) >= '" . $bfr3Month . "'";
} else if ($_REQUEST['type'] == '2') {
    $cond = " DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($_REQUEST['from_d'])) . "' AND '" . date('Y-m-d', strtotime($_REQUEST['to_d'])) . "'";
} else if ($_REQUEST['type'] == '3') {
    $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "'";
} else if ($_REQUEST['type'] == '4') {
    //echo $_REQUEST['type'];
    $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and  DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($_REQUEST['from_d'])) . "' AND '" . date('Y-m-d', strtotime($_REQUEST['to_d'])) . "'";
} else if ($_REQUEST['type'] == '5') {

    if ($_REQUEST['cond'] == '1') {
        $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and DATE(ap.appointment_dt) = '" . $today . "'";
    } else if ($_REQUEST['cond'] == '2') {
        $weekArr = week_start_end_by_date($currTime);
        $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and DATE(ap.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'";
    } else if ($_REQUEST['cond'] == '3') {
        $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and DATE(ap.appointment_dt) >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY
AND DATE(ap.appointment_dt) < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY";
    } else if ($_REQUEST['cond'] == '4') {
        $currMonth = date('n', $currTime);
        $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and MONTH(ap.appointment_dt) = '" . $currMonth . "' ";
    } else if ($_REQUEST['cond'] == '5') {

        $currMonth = date('n', $currTime);
        $prevMonth = $currMonth - 1;
        if ($prevMonth == 0)
            $prevMonth = 12;

        $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and MONTH(ap.appointment_dt) = '" . $prevMonth . "'";
    }
    else if ($_REQUEST['cond'] == '6') {
        $bfr3Month = date('Y-m-d', strtotime("-3 months"));
        $cond = " ap.mas_id = '" . $_REQUEST['for_u'] . "' and DATE(ap.appointment_dt) >= '" . $bfr3Month . "'";
    }
}
?>
<script type="text/javascript">
    $(document).ready(function () {

        $('.detail_view_finance').on('click', function () {
            var dis = $(this).attr('id');
//            alert(dis);
            $('#appt_details_finance').html('');
            $.ajax({
                type: "POST",
                url: "getBookingDetailsfinance.php",
                data: {booking: dis},
//                dataType: "JSON",
                success: function (result) {
//                    alert(result);
                    $('#appt_details_finance').html(result);
                }
            });

        });


    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 11, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null]});
    });
</script>
<style>
    .mainrev{

        border:solid 1px black;
        margin-top: 20px;
        height: 100px;
        background-color: #2a2a2a;
        margin-bottom: 30px;
    }
    .revenue1{
        background-color:white;float:left;color:white;border:solid 1px #2a2a2a;margin-left: 80px;margin-top: 10px;
    }
    .revenue2{
        background-color:white;float:left;margin-left:30px;color:white !important;border:solid 1px #2a2a2a;margin-top: 10px;
    }

    .revenue3{
        background-color:white;float:left;margin-left:30px;color:white;border:solid 1px #2a2a2a;margin-top: 10px;
    }
    .revenue4{
        background-color:white;float:left;margin-left:30px;color:white;border:solid 1px #2a2a2a;margin-top: 10px;
    }
    .labeltotal{
        width:160px;height:25px;margin-left:2px;margin-right:2px;margin-top:2px;padding-left:3px;padding-top:3px;padding-bottom:3px;color:white;font-size:12px;background-color:rgb(183, 183, 190);;
    }
    .resulttotal{
        width:160px;height:45px;padding-top:10px;color:black;font-size:10px;padding-left:50px;font-size: 20px; 
    }
</style>
<?php
if ($cityid == '' and $companyids == '') {
    $accQry1 = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . "order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
} else if ($cityid != '' && $companyids == '') {
    $accQry1 = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . " and d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
} else if ($cityid == '' && $companyids != '') {
    $accQry1 = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . " and ap.mas_id in(select mas_id from master where company_id =" . $companyids . " ) order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
} else {
    $accQry1 = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . " and d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . " and company_id = " . $companyids . ")) order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
}
$resulttotal = mysql_query($accQry1, $dbHistory->conn);
//echo $accQry;
$revenue = $commision1 = $stripe_fee1 = $transferAmt1 = 0;
$i = 1;

$statusArr = array("Status unavailable.", "Booking requested", 'Driver accepted.',
    'Driver rejected.', 'Passenger have cancelled.', 'Driver have cancelled.', 'Driver is on the way.', 'Driver arrived.',
    'Booking started.', 'Booking completed.', 'Booking expired.');

while ($row1 = mysql_fetch_assoc($resulttotal)) {

    $revenue += $row1['amount'];

    $commision1 += $row1['amount'] * (10 / 100);

    $stripe_fee1 += (float) ($row1['amount'] * (2.9 / 100)) + 0.3;

    $transferAmt1 += (float) (($row1['amount'] - ($row1['amount'] * (10 / 100)) - (float) (($row1['amount'] * (2.9 / 100)) + 0.3)));
}
?>
<div class="mainrev">
    <div class="revenue1" >
        <div class="labeltotal">TOTAL REVENUE</div>
        <div class="resulttotal"><?php echo '&euro;' . number_format($revenue, 2); ?></div>
    </div>
    <div class="revenue2" >
        <div class="labeltotal">TOTAL COMMISSION</div>
        <div class="resulttotal"><?php echo '&euro;' . number_format($commision1, 2); ?></div>
    </div>
    <div class="revenue3" >
        <div class="labeltotal">TOTAL STRIPE </div>
        <div class="resulttotal"><?php echo '&euro;' . number_format($stripe_fee1, 2); ?></div>
    </div>
    <div  class="revenue4" >
        <div class="labeltotal">TOTAL DRIVER EARNINGS</div>
        <div class="resulttotal"><?php echo '&euro;' . number_format($transferAmt1, 2); ?></div>
    </div>

</div>

<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">




        <tr>
            <th>APP DATE</th>
            <th>DATE</th>
            <th>PAYMENT METHOD</th>   
            <!--<th>TRANSACTION ID</th>--> 
            <th>DRIVER ID</th>
            <th>BILL ID</th>
            <th>DRIVER NAME</th>
            <th>COMMISSION</th>
            <th>DRIVER EARNING</th>

            <th>STRIPE AMOUNT</th>

            <th>DETAILS</th>
        </tr>

    </thead>
    <tbody style="font-size: 12px;">

<?php ?> 
<?php
if ($cityid == '' and $companyids == '') {
    $accQry = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . "order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
} else if ($cityid != '' && $companyids == '') {
    $accQry = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . " and d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
} else if ($cityid == '' && $companyids != '') {
    $accQry = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . " and ap.mas_id in(select mas_id from master where company_id =" . $companyids . " ) order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
} else {
    $accQry = "select ap.appointment_id,ap.amount,ap.inv_id,ap.txn_id,ap.payment_type,ap.complete_dt,ap.status,ap.mas_id,d.first_name,ap.appointment_dt,ap.address_line1,ap.drop_addr1,ap.distance_in_mts,ap.duration,ap.type_id,ap.apprxAmt from appointment ap,master d where ap.mas_id=d.mas_id and ((ap.status = 4 and ap.cancel_status = 3) or (ap.status = 5 and ap.cancel_status !=7) or (ap.status=9)) and ap.payment_status = " . $_REQUEST['status_payment'] . " and" . $cond . " and d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . " and company_id = " . $companyids . ")) order by ap.appointment_dt DESC";   // (select Title from workplace where type_id=ap.type_id) as type_name,
}
$result1 = mysql_query($accQry, $dbHistory->conn);
//echo $accQry;

$i = 1;

$statusArr = array("Status unavailable.", "Booking requested", 'Driver accepted.',
    'Driver rejected.', 'Passenger have cancelled.', 'Driver have cancelled.', 'Driver is on the way.', 'Driver arrived.',
    'Booking started.', 'Booking completed.', 'Booking expired.');

while ($row = mysql_fetch_assoc($result1)) {

    $commision = $row['amount'] * (10 / 100);

    $stripe_fee = (float) ($row['amount'] * (2.9 / 100)) + 0.3;

    $transferAmt = (float) (($row['amount'] - $commision - $stripe_fee));
    //$totalamount=sum($row['amont']);
    ?>

            <tr>
                <td><?php echo $row['appointment_dt']; ?></td>
                <td><?php echo $row['complete_dt']; ?></td>
                <td><?php
            if ($row['payment_type'] == 1)
                echo 'card';
            else
                echo 'cash';
            ?></td>
                <!--<td><?php // echo $row['txn_id'] ?></td>-->
                <td><?php echo $row['mas_id']; ?></td>
                <td><?php echo $row['inv_id'] ?></td>
                <td><?php
            echo $row['first_name'];
            ?></td>
                <td><?php
            echo $commision;
            ?></td>
                <td><?php
            echo $transferAmt;
            ?></td>
                <td><?php
            echo $stripe_fee;
            ?></td>

                <td class="detail_view_finance" id="<?php echo $row['appointment_id']; ?>"><a href="#modal_default_14" data-toggle="modal" class="btn btn-default" style="background: #555;color: white;">Details</a></td>


            </tr>
            <?php
            $i++;
        }
        ?> 

    </tbody>
</table>         

<div class="modal modal-white" id="modal_default_14" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 120%;">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Appointment Details</h4>
            </div>                
            <div class="modal-body clearfix">
                <div id="appt_details_finance"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>              
            </div>
        </div>
    </div>
</div> 