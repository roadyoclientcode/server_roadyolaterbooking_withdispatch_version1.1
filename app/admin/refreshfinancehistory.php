<?php
//echo 1;
//return false;
//require('../Models/ConDB.php');
//$db2 = new ConDB();
//echo 1;

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 11, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null, null]});
    });
</script>
<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">
        <tr>
            <th>S NO</th>
            <th>BOOKING ID</th>
            <th>DATE</th>
            <th>TIME</th>   
            <th>PASSENGER</th> 
            <th>DRIVER</th>
            <th>PAYMENT TYPE</th>
            <th>CHARGE ID</th>
            <th>AMOUNT</th>
            <th>STATUS</th>
            <th>DETAILS</th>
        </tr>
    </thead>
    <tbody style="font-size: 12px;">

        <?php
        //$accQry = "select ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status > 4 order by ap.appointment_id DESC";
        if ($cityid == '' and $companyids == '') {
            $accQry = "select ap.appointment_dt,ap.payment_type,ap.payment_status,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where  ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 order by ap.appointment_id DESC";
        } else if ($cityid != '' && $companyids == '') {
            $accQry = "select ap.appointment_dt,ap.payment_type,ap.payment_status,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 order by ap.appointment_id DESC";
        } else if ($cityid == '' && $companyids != '') {
            $accQry = "select ap.appointment_dt,ap.payment_type,ap.payment_status,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where d.company_id IN((" . $companyids . ")) AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 order by ap.appointment_id DESC";
        } else {
            $accQry = "select ap.appointment_dt,ap.payment_type,ap.payment_status,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . " and company_id = " . $companyids . "))  AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 order by ap.appointment_id DESC";
        }
        $result1 = mysql_query($accQry, $db1->conn);
//echo $accQry;
        $i = 1;

        $statusArr = array("Status unavailable.", "Booking requested", 'Driver accepted.',
            'Driver rejected.', 'Passenger have cancelled.', 'Driver have cancelled.', 'Driver is on the way.', 'Driver arrived.',
            'Booking started.', 'Booking completed.', 'Booking expired.');

        while ($row = mysql_fetch_assoc($result1)) {
            ?>

            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['appointment_id']; ?></td>
                <td><?php echo date('d-m-Y', strtotime($row['appointment_dt'])); ?></td>
                <td><?php echo date('h:i A', strtotime($row['appointment_dt'])); ?></td>
                <td><?php echo $row['pat_email'] ?></td>
                <td><?php echo $row['doc_email']; ?></td>
                <td><?php
                    if ($row['payment_type'] == 1) {
                        echo "Card";
                    } else {
                        echo "Cash";
                    };
                    ?></td>
                <td><?php
                    if ($row['payment_type'] == 2) {
                        echo "Cash";
                    } else {
                        echo $row['inv_id'];
                    };
                    ?></td>
                <td><?php
                    if ($row['status'] == 9 || $row['cancel_status'] == 3 || $row['cancel_status'] = 4) {
                        echo $row['amount'];
                    } else {
                        echo 'NILL';
                    }
                    ?></td>
                <td><?php
                    if ($row['payment_status'] == '2') {
                        echo "Disputed";
                    } else {
                        echo (empty($statusArr[$row['status']])) ? $statusArr[0] : $statusArr[$row['status']];
                    }
                    ?></td>
                <?php $getinv = $row['inv_id'] . '.' . 'pdf'; ?>
                <td>
                    <?php
                    if ($row['status'] == '9') {
                        ?>
                        <a href="<?php echo $db1->picHost; ?>../getPDF.php?apntId=<?php echo $row['appointment_id']; ?>" target="_blank"><button type="submit" value="view">View</button></a>
                    <?php }
                    ?>
                </td>

            </tr>
            <?php
            $i++;
        }
        ?> 

    </tbody>
</table>         

