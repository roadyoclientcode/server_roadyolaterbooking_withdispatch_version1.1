<?php
//echo 1;
//return false;
//require('../Models/ConDB.php');
//$db2 = new ConDB();
//echo 1;

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>
<script type="text/javascript">
    $(document).ready(function() {
<?php if ($status == '1') { ?>
            if ($("table.sortable").length > 0)
                $("table.sortable").dataTable({"iDisplayLength": 13, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null]});
<?php } else { ?>
            if ($("table.sortable").length > 0)
                $("table.sortable").dataTable({"iDisplayLength": 13, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null]});
<?php } ?>
    });
</script>
<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">
        <tr>
            <th>DISPUTE ID</th>
            <th>PASSENGER ID</th>

            <th>PASSENGER NAME</th>   
            <th>DRIVER ID</th> 
            <th>DRIVER NAME</th>
            <th>DISPUTE MESSAGE</th>
            <?php if ($status == '2') echo "<th>MANAGEMENT NOTE</th>"; ?>
            <th>DISPUTED DATE</th>
            <th>APPOINTMENT ID</th>
            <th>SELECT</th>
        </tr>
    </thead>
    <tbody style="font-size: 12px;">

        <?php
        if ($cityid == '' && $companyids == '') {
            $accQry = "select rep.admin_note,mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC";
        } else if ($cityid != '' && $companyids == '') {
            $accQry = "select rep.admin_note,mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id and mas.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . "))  and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC";
        } else if ($cityid == '' && $companyids != '') {
            $accQry = "select rep.admin_note,mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id and mas.company_id = " . $companyids . "  and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC";
        } else {
            $accQry = "select rep.admin_note,mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id and mas.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) and mas.company_id = " . $companyids . " and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC";
        }



        /*
          if($cityid == '')
          {
          $accQry = "select mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC";

          }
          else
          {
          $accQry = "select mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id and mas.company_id IN((SELECT company_id FROM company_info WHERE city = ".$cityid."))  and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC";
          } */
        $result1 = mysql_query($accQry, $db1->conn);

//             echo $accQry;
//$result = mysql_query("select r.article_id,r.title,r.description,r.Status,r.thumbnail,r.Status,r.posted_dt_time,u.Firstname,u.LastName,a.path from articleimages a , report r,users u where r.Uid=u.Uid and a.article_id=r.article_id ", $db1->conn);
        $i = 1;
        while ($row = mysql_fetch_assoc($result1)) {
            ?>
            <tr id="pat_rows<?php echo $i; ?>">
                <td><?Php echo $row['report_id']; ?></td>
                <td><?php echo $row['slave_id']; ?></td>

                <td><?Php echo $row['slv_name'] . ' ' . $row['slv_lname']; ?></td>
                <td><?php echo $row['mas_id']; ?></td>
                <td><?Php echo $row['mas_fname'] . ' ' . $row['mas_lname']; ?></td>
                <td><?Php echo $row['report_msg']; ?></td>
                <?php if ($status == '2') echo "<td>" . $row['admin_note'] . "</td>"; ?>
                <td><?Php echo date("d/m/Y H:i:s", strtotime($row['report_dt'])); ?></td>

                <td><?Php echo $row['appointment_id']; ?></td>
                <td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['report_id']; ?>" style="background: white;height: 20px;width: 12px;" /></td>
            </tr>
            <?php
            $i++;
        }
        ?> 

    </tbody>
</table>   


