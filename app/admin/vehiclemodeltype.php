<?php
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>
<style>
    #regCompanyErr,#regVehicleErr,#regTitleErr,#regVehicleModelErr,#regSeatingErr,#regVehicleregErr,#regLicenseErr,#regColorErr,#regExpireErr,#regExpireInsurErr,#regExpirePermitErr
    {
        color:red;
    }
</style>
<!---Code for adding images-->		
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>
<div class="modal modal-draggable" id="modal_default_9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">ADD VEHICLE TYPE</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">TYPE NAME:</label> <input type="text"  required="required"  name="vehicletypemodel" style="width: 200px;display: inline;" id="vehicletypemodel">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="addButton">ADD</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="close_modal_1">CANCEL</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">





    <div style="float:right;">

<!--        <p> 
            <label for="text" class="youpasswd" >VEHICLE TYPE MODEL<span style="color:red">*</span></label>
            <input id="vehicletypemodel" name="vehicletypemodel" required="required" class="vehicletypemodel" type="text" placeholder="eg. KA-05/1800" /> 
            <span id="regLicenseErr"></span>
        </p>-->
        <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active"><a href="#modal_default_9" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    
        <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="2" data-msg="delete">DELETE</button>   


    </div>
    <!-- <div style="float:none;"></div>
    <!--<div id="refresh_table"></div>-->
    <div calss="vehicletype">
        <div id="refresh_table1"><?php include('getvehiclemodeldetails.php'); ?></div>
    </div>
</div> 
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#addButton').click(function () {

            var typename = $('#vehicletypemodel').val();
            if (typename == '') {

                alert("enter vehicle type");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "addvehicletypemodel.php",
                data: {item_type: 1, item_list: typename, add_val: 1},
                dataType: "JSON",
                success: function (result)
                {
                    $('#close_modal_1').trigger('click');

                    // alert(result.qrys);
                    //alert(result.flag);
                    if (result.flag == 0)
                    {
                        $('#refresh_table1').load('getvehiclemodeldetails.php', {}, function () {
//                                alert('data refreshed');
                        });


                    } else
                    {
                        alert('Error occured in adding vehicle type');
                    }
                },
                error: function ()
                {
                    alert('Error occured1');
                }
            });
        });

        $('#title').change(function () {
            var adv_id = $('#title').val();
            if (adv_id != null || adv_id != '')
                $('#vehiclemodel').load('getvehicle.php', {adv: adv_id});
        });


        $('.resetPassword').click(function () {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });

        $('#change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
//                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#ActiveButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast vechile type in the list');
            } else if (confirm('Are you confirm to  ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "addvehicletypemodel.php",
                    data: {item_type: 1, item_list_res: values, add_val: 2},
                    dataType: "JSON",
                    success: function (result) {
                        //  alert(result.message);
                        // alert(result.qrys);
                        // alert(result.flag);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                            $('#refresh_table1').load('getvehiclemodeldetails.php', {}, function () {
//                                alert('data refreshed');
                            })
                        }
                    }
                });
            }
        });



        $('#EditButton').click(function () {
            var dis = $(this);

            var count = 0;
            var values = $('input:checkbox:checked.custom_check').map(function () {
                count++;
                return this.value;
            }).get();
            if (values == '')
            {
                alert("please select one vehicle");
                return false;
            } else if (count > 1) {

                alert("please select only one vehicle");
                return false;
            }
            else
            {
                $('#heading-main').text('Edit a vehicle');
                $.ajax({
                    type: "POST",
                    url: "edit_vehicles.php",
                    data: {item_type: 5, item_list: values},
                    dataType: "JSON",
                    success: function (result) {

                        if (result.errFlag == 0) {

                            $('.workplace_id_hidden').val(result.workplace_id);
                            $('.certificate_hidden').val(result.certificate);
                            $('.insurcertificate_hidden').val(result.insurcertificate);
                            $('.carriagecertificate_hidden').val(result.carriagecertificate);
                            $('.editvehiclemodel').val(result.Vehicle_Model);
                            $('#companyname').val(result.company);
                            $('.editvechiletype').val(result.Vehicle_Type);
                            $('.editvechileregno').val(result.Vehicle_Reg_No);
                            $('.editinsuranceno').val(result.Vehicle_Insurance_No);
                            $('.editseating').val(result.Vehicle_Seating);
                            $('.editlicenceplaetno').val(result.License_Plate_No);
                            $('.editvechilecolor').val(result.Vehicle_Color);
                            $('.edit_title').val(result.Title);
                            $('.editvehicletype').val(result.type_id);
                            $('#expirationinsurance').val(result.expirydate);
                            $('#expirationrc').val(result.expirydate);
                            $('#expirationinsurance').val(result.vehicle_insurance_expiry_dt);
                            $('#expirationpermit').val(result.Vehicle_Insurance_Dt);

                            $('#editdataval').val(result.errFlag);

                            //  $('.editgetvechiletype').val(result.type_id);
                        } else {
                            alert("Error in getting values");
//                        alert(result.qry);
//						alert(result.msg);
                        }
                    },
                    error: function (res)
                    {
                        alert("sorry,Error occured");
                    }
                });
            }
        });


        $("#datasubmit").click(function () {

            var cityidres = <?php echo json_encode($cityid); ?>;
            var companyres = <?php echo json_encode($companyids); ?>;

            var count = 0;
            if ($('#companyname').val() == "NULL")
            {
                $('#companyname').focus();
                $('#regCompanyErr').html("<?php echo SA_choose_company; ?>");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#getvechiletype').val() == "NULL")
            {
                $('#getvechiletype').focus();
                $('#regVehicleErr').html("<?php echo SA_choose_vehicle_type; ?>");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#title').val() == "NULL")
            {
                $('#title').focus();
                $('#regTitleErr').html("<?php echo SA_choose_vehicle_make; ?>");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#vehiclemodel').val() == "NULL")
            {
                $('#vehiclemodel').focus();
                $('#regVehicleModelErr').html("<?php echo SA_choose; ?>");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#seating').val() === "")
            {
                $('#seating').focus();
                $('#regSeatingErr').html("Seating capacity is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#vechileregno').val() === "")
            {
                $('#vechileregno').focus();
                $('#regVehicleregErr').html("<?php echo SA_vehicle_reg_no; ?>");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#licenceplaetno').val() === "")
            {
                $('#licenceplaetno').focus();
                $('#regLicenseErr').html("<?php echo SA_vehicle_licence; ?>");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#vechilecolor').val() === "")
            {
                $('#vechilecolor').focus();
                $('#regColorErr').html("<?php echo SA_vh_color; ?>");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#expirationrc').val() === "2014-05-01")
            {
                $('#expirationrc').focus();
                $('#regExpireErr').html("<?php echo SA_vehicle_expiration; ?>");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#expirationinsurance').val() === "2014-05-01")
            {
                $('#expirationrc').focus();
                $('#regExpireInsurErr').html("<?php echo SA_vehicle_expiration; ?>");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#expirationpermit').val() === "2014-05-01")
            {
                $('#expirationpermit').focus();
                $('#regExpirePermitErr').html("<?php echo SA_vehicle_expiration; ?>");
                return false;
            }
            if (count > 0)
                return false;

            var formElement = document.getElementById("addVehicleForm");
            var formData = new FormData(formElement);
            if ($('#editdataval').val() != '0')
            {
                $.ajax({
                    url: "test_upload.php",
                    type: "POST",
                    data: formData,
                    dataType: "JSON",
                    async: false,
                    success: function (result) {
                        //  alert(result);
                        if (result.flag == 0) {
                            $('#close_modal').trigger('click');
                            $('#refresh_table').load('refreshvechiledetail.php', {type: 5, cityid: cityidres, companyid: companyres});
                        } else {
                            alert('Error occured');
//						 alert(result.msg);
                        }
                    },
                    error: function (result) {
//                    alert('test1')
                        //  alert(result);
                        alert('Error occured');
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            else
            {
                $.ajax({
                    url: "edit_vehicles_add.php",
                    type: "POST",
                    data: formData,
                    dataType: "JSON",
                    async: false,
                    success: function (result) {
                        //  alert(result);
                        if (result.flag == 0) {
                            $('#close_modal').trigger('click');
                            $('#refresh_table').load('refreshvechiledetail.php', {type: 5, cityid: cityidres, companyid: companyres});
                        } else {
//                            alert('Error occured');
                            alert(result.msg);
                        }
                    },
                    error: function (result) {
//                    alert('test1')
                        //  alert(result);
                        alert('Error occured');
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

            e.preventDefault();
        });


    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>




