<?php

require('../Models/config.php');
require('../Models/Pubnub.php');

$pubnub = new Pubnub(PUBNUB_PUBLISH_KEY, PUBNUB_SUBSCRIBE_KEY);

$con = new Mongo();

$con = new MongoClient("mongodb://" . MONGODB_HOST . ":" . MONGODB_PORT . "");
$mongoDB = $con->selectDB(MONGODB_DB);

$booking_route = $mongoDB->selectCollection('booking_route');
$location = $mongoDB->selectCollection('location');
if ($_GET['data'] == 'updateStatus') {
    print_r(json_encode(array('test' => 'done')));
    $location->update(
            array('email' => $_GET['email']), array(
        '$set' => array('status' => (int) $_GET['status']),), array("upsert" => true)
    );
}
if ($_GET['data'] == 'updateLocation') {
    $cond = array("email" => strtolower($_GET['e_id']));

    $newdata = array('$set' => array("location" => array("longitude" => (float) $_GET['lg'], "latitude" => (float) $_GET['lt']), 'lastTs' => time()));

    $location->update($cond, $newdata);

    if (isset($_GET['bid']) && (int) $_GET['bid'] > 0 && (int) $_GET['bid'] != '') {
        $data = array("longitude" => (double) $_GET['lg'], "latitude" => (double) $_GET['lt']);
        if (is_array($location->findOne(array('bid' => (int) $_GET['bid'])))) {
            $booking_route->update(
                    array('bid' => (int) $_GET['bid'], 'route' => array('$ne' => $data))
                    , array('$push' => array('route' => $data))
                    , array("upsert" => false)
            );
        } else {
            $location->insert(array('bid' => (int) $_GET['bid'], 'route' => array($data)));
        }
    }
}

if ($_GET['data'] == 'getDrivers') {
    $Testing = $mongoDB->selectCollection('Testing');
    $Testing->insert($_GET);
    $foundEs = $typesData = $foundNew = array();
//print_r('here->');
//                if ($args['st'] == '3') { // getting data for live drivers
    $resultArr = $mongoDB->selectCollection('$cmd')->findOne(array(
        'geoNear' => 'location',
        'near' => array(
            (double) $_GET['lg'], (double) $_GET['lt']
        ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137,
        'query' => array('status' => 3))//, 'type' => (int) $typeToGet
    );

    foreach ($resultArr['results'] as $res) {
        $doc = $res['obj'];

        if (count($foundEs) < 6)
            $foundEs[] = $doc['email'];
    }


    if (count($foundEs) > 0)
        $return = array('a' => 2, 'flag' => 0, 'es' => $foundEs); //, 't' => $resultArr);
    else
        $return = array('a' => 2, 'flag' => 1);

//    print_r($return);
    $pubRes[] = $pubnub->publish(array(
        'channel' => $_GET["chn"],
        'message' => $return
    ));
}
