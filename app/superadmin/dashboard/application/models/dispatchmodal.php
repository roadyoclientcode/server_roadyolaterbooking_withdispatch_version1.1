<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

class Dispatchmodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
    }

    function validateSuperAdmin() {

        $email = $this->input->post("email");
        $password = $this->input->post("password");

        $queryfordisp = $this->db->get_where('dispatcher', array('dis_email' => $email, 'dis_pass' => $password));
        $res = $queryfordisp->row();


        if ($queryfordisp->num_rows > 0) {
            $tablename = 'dispatcher';
            $LoginId = 'dis_id';
            $sessiondata = $this->setsessiondata($tablename, $LoginId, $res,$email,$password);
            $this->session->set_userdata($sessiondata);
            return true;
        }

        return false;
    }


      function getDtiverDetail(){

        $did = $this->input->post("did");

        $queryM =$this->db->query("select * from master where mas_id ='".$did."'")->result();
        $queryV=$this->db->query("select w.Title,w.Vehicle_Model,vm.vehiclemodel,vt.vehicletype from master m,vehicleType vt,vehiclemodel vm,workplace w where m.mas_id='".$did."' and m.workplace_id=w.workplace_id and w.Title =vt.id and w.Vehicle_Model = vm.id")->result();
        $queryapp=$this->db->query("select appointment_id,appointment_dt,address_line1,drop_addr1 from appointment  where mas_id='".$did."' and  appt_type=2 and status in(1,2)")->result();


          foreach($queryM as $master){
              $name = $master->first_name.$master->last_name;
              $mobile = $master->mobile;
              $license = $master->license_num;
              $profile = $master->profile_pic;
          }
 foreach($queryV as $vehicle){
     $vtype = $vehicle->vehicletype;
     $vmodel=$vehicle->vehiclemodel;
 }

          if($profile){
              $img = base_url()."../pics/".$profile;
          }
          else{
              $img = "http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif";
          }
        $html ='<div id="quickview" class="quickview-wrapper open" data-pages="quickview" style="max-height: 487px;margin-top: 39px;">

<ul class="nav nav-tabs" style="padding: 0 14px;">
    <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                                                                 <span class="col-xs-height col-middle">
                                                                <span class="thumbnail-wrapper d32 circular bg-success">
                                                                    <img width="34" height="34" alt="" data-src-retina="'.$img.'" data-src="'.$img.'" src="'.$img.'" class="col-top">
                                                                </span>
                                                                </span>
        <p class="p-l-20 col-xs-height col-middle col-xs-12">
            <span class="text-master" style="color: #ffffff !important;">'.$name.'</span>
            <span class="block text-master hint-text fs-12" style="color: #ffffff !important;">+91'.$mobile.'</span>
        </p>
    </a>


</ul>
<a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close" style="color: #ffffff ! important;"></i></a>

<div class="tab-content" style="top: 21px !important;">


<div class="list-view-group-container" >

<ul>

<li class="chat-user-list clearfix">
        <div class="form-control">
            <label class="col-sm-5 control-label">Model</label><label class="col-sm-7 control-label">'.$vmodel.'</label>
        </div>

    </li>
    <li class="chat-user-list clearfix">

        <div class="form-control">
            <label class="col-sm-5 control-label">Car Type</label><label class="col-sm-7 control-label">'.$vtype.'</label>
        </div>


    </li>

    <li class="chat-user-list clearfix">

        <div class="form-control">
            <label class="col-sm-5 control-label">License no</label><label class="col-sm-7 control-label">'.$license.'</label>
        </div>

    </li>


</ul>


<div class="list-view-group-container" style="overflow-y: scroll;max-height: 314px;">
<div class="list-view-group-header text-uppercase" style="background-color: #f0f0f0;padding: 10px;">
            ASSIGNED JOBS</div>';
        foreach($queryapp as $result){

            $html.='<div style="overflow: auto;background: #fff;">
    <ul style="margin-top: 15px;">

        <li class="chat-user-list clearfix">


            <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                <div class="pull-right" style="margin: 5px 5px 0px 11px;width: 157px;">
                '.date("M d Y g:i A", strtotime($result->appointment_dt)).'

            </div>
                <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                '.$result->appointment_id.'

            </div>
                <div class="item-description" style="">

                    <ul>

                        <li class="chat-user-list clearfix">


                             <div class=""  style="border: 1px solid rgba(0, 0, 0, 0.07);">
                             <p style="padding: 8px;">'.$result->address_line1.'</p>


                            </div>


                        </li>
                        <li class="chat-user-list clearfix">



                        <div class="" style="border: 1px solid rgba(0, 0, 0, 0.07);">
                             <p style="padding: 8px;">'.$result->drop_addr1.'</p>


                            </div>
                        </li>

                    </ul>
                </div>
            </div>



        </li>


    </ul>

</div>';

        }




        $html.='<div style="margin-top: 16px;margin-left: 107px;"><button class="btn btn-tag   btn-tag-light btn-tag-rounded m-r-20" onclick="assignDriver('.$did.')">Assign</button></div></div></div></div></div>';


        echo json_encode(array('html' => $html));

    }

    function get_all_drivers(){

        $typeQry = ",(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type ";

        $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas where  mas.status IN (3)  order by mas.mas_id DESC";

        $query = $this->db->query($accQry)->result();

        $arraytosend = array();

        $code = '';
        foreach ($query as $result) {

            if($result->profile_pic)
                $img = base_url()."../pics/".$result->profile_pic;
            else
                $img = "http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif";

            $code .= '<p style="float: left;">
                        <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#" onclick="get_driver_data('.$result->mas_id.')" >
                                                                                            <span class="col-xs-height col-middle">
                                                                                            <span class="thumbnail-wrapper d32 circular bg-success" >';

            $code .='<img width="34" height="34" alt="" data-src-retina="'.$img.'" data-src="'.$img.'" src="'.$img.'" class="col-top">';

            $code .= '</span></span><p class="p-l-10 col-xs-height col-middle" style="">
                                <span class="text-master"> '.$result->first_name.'</span>
                                <span class="block text-master hint-text fs-12">'.$result->mobile.'</span>
                            </p></a></p>';



            $arraytosend[] = array(
                "data" =>$code,
            );
            $code = '';
        }

        echo json_encode(array('html' => $arraytosend));




    }
    function get_all_customers(){
        $query = $this->db->query("select * from slave Limit 200")->result();

        return $query;

    }

    function  add_appointment(){


//        echo  $this->input->post('appfare');
//        exit();

        $v=0;
        $date = $this->input->post('date')." ".$this->input->post('hours').":".$this->input->post('min').":00"; //'05/22/2015 07:03:41';
        $addindb= date("Y-m-d H:i:s", strtotime($date));


        if($this->input->post('slave_id') == ''){
            $data = array(
                'first_name' => $this->input->post('fname'),
                'email' => $this->input->post('email'),
                'last_name' => $this->input->post('lname'),
                'phone' => $this->input->post('phone')
            );
            $this->db->insert('slave', $data);
            $insert_id = $this->db->insert_id();


            $data2 = array(
                'appt_type' =>'2',
                'address_line1' => $this->input->post('address_line1'),
                'appt_lat' => $this->input->post('pickup_lat'),
                'appt_long' => $this->input->post('pickup_long'),
                'drop_addr1' => $this->input->post('drop_addr1'),
                'drop_lat' => $this->input->post('drop_lat'),
                'drop_long' => $this->input->post('drop_long'),
                'distance_in_mts' => ($this->input->post('distance')*1000),
                'apprxAmt' => $this->input->post('appfare'),
                'B_type' => 2,
                'status'=>1,
                'appointment_dt' => $addindb,//date('Y-m-d H:i:s',$this->input->post('date').$this->input->post('time') ),
                'slave_id' => $insert_id,
                'type_id' =>$this->input->post('vehicle_type')
            );

//            $v = $this->db->query("insert into  appointment_later(appt_type,address_line1,appt_lat,appt_long,drop_addr1,drop_lat,drop_long,slave_id) ");
            $this->db->insert('appointment', $data2);



            $v = $this->db->insert_id();
        }
       else{

           $data2 = array(
               'appt_type' =>'2',
               'address_line1' => $this->input->post('address_line1'),
               'appt_lat' => $this->input->post('pickup_lat'),
               'appt_long' => $this->input->post('pickup_long'),
               'drop_addr1' => $this->input->post('drop_addr1'),
               'drop_lat' => $this->input->post('drop_lat'),
               'drop_long' => $this->input->post('drop_long'),
               'distance_in_mts' => ($this->input->post('distance')*1000),
               'apprxAmt' => $this->input->post('appfare'),
               'B_type' => 2,
               'status'=>1,
               'appointment_dt' => $addindb,
               'slave_id' => $this->input->post('slave_id'),
               'type_id' =>$this->input->post('vehicle_type')
           );

//            $v = $this->db->query("insert into  appointment_later(appt_type,address_line1,appt_lat,appt_long,drop_addr1,drop_lat,drop_long,slave_id) ");
           $this->db->insert('appointment', $data2);
           $v = $this->db->insert_id();
       }



        if($v != "0"){
            echo json_encode(array('response' => 1,'bid' => $v));
        }else{
            echo json_encode(array('response' => 0));
        }

    }


    // domid  is nothing but html object id
        function get_slave_data(){

            $search_with = $this->input->post('domvalue');
            $domid = $this->input->post('domid');

            $query = $this->db->query("select email,first_name,last_name,phone,slave_id from slave where ".$domid." ='".$search_with."' ")->result();

            foreach($query as $result){
                $fistname = $result->first_name;
                $lastname = $result->last_name;
                $phone = $result->phone;
                $slave_id = $result->slave_id;
                $email = $result->email;
            }
            echo json_encode(array('fname' => $fistname,"lname" => $lastname,"phone" => $phone,'slave_id' => $slave_id,'email' => $email,"tet" => "select email,first_name,last_name,phone,slave_id from slave where ".$domid." ='".$search_with."'"));

        }


    function get_salve_details($slave_id = '',$app_id = ''){


        $query=$this->db->query("SELECT a.drop_long,a.drop_lat,a.appt_lat,a.appt_long,a.status,s.first_name,s.last_name,s.email,s.phone,a.appointment_id,a.appt_lat,a.appt_long,a.address_line1,a.address_line2,a.drop_addr1,a.apprxAmt,a.drop_addr2,a.appointment_dt,a.distance_in_mts,a.amount,a.type_id FROM appointment a,slave s WHERE a.appt_type='2' and a.slave_id = s.slave_id and a.slave_id = '".$slave_id."' and a.appointment_id = '".$app_id."'")->result();
//        SELECT a.status,s.first_name,s.last_name,s.email,s.phone,a.appt_lat,a.appt_long,a.address_line1,a.address_line2,a.drop_addr1,a.drop_addr2,a.appointment_dt,a.distance_in_mts,a.amount,vm.vehiclemodel,wt.type_name FROM appointment a,slave s,workplace w,workplace_types wt,vehiclemodel vm WHERE a.appt_type='2' and a.slave_id = s.slave_id and a.type_id = w.workplace_id and w.type_id =wt.type_id and w.Vehicle_Model=vm.id and a.slave_id = '".$slave_id."'
        return $query;


    }

    function  get_driver_Data(){

        $driverid = $this->input->post('did');

            $query = $this->db->query("select a.appointment_id,a.complete_dt,a.appointment_dt,a.address_line1,a.address_line2,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,d.first_name as driver_fname,d.last_name as driver_lname,p.first_name as pessanger_fname,p.last_name as pessanger_lname,p.phone,a.status from appointment a,master d,slave p where a.slave_id=p.slave_id and d.mas_id=a.mas_id and a.status IN (1,2,3,4,5,6,7,8,9) and d.mas_id = '".$driverid."'and appointment_dt >= NOW() and appt_type = 2 order by a.appointment_id desc")->result();


        $slno = 1;
        $succ =1;
        $code = 'No Jobs found !';
        $arraytosend = $arraytosend2 = array();
        foreach ($query as $result) {
            $succ =2;
            $code .= '<tr role="row"  class="gradeA odd">
                    <td id = "d_no" class="v-align-middle sorting_1"  style="padding: 7px!important;font-size: 2.5px;width: 0px;"> <p> '.$slno.' </p></td>
            <td id = "d_no" class="v-align-middle sorting_1"  style="padding: 7px!important;font-size: 2.5px;"> <p> '.$result->appointment_id.'</p></td>
            <td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;">'.$result->pessanger_fname.$result->pessanger_lname.'</td>
            <td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;">'.$result->phone.' </td>
            <td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;"> '.date("M d Y g:i A", strtotime($result->appointment_dt)).'</td>
            <td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;">'.$result->address_line1.$result->address_line2.'</td>
            <td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;">'.$result->drop_addr1.$result->drop_addr2.'</td>
            <td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;">BMW</td>';

            if ($result->status == '1')
                $status = 'Appointment requested';
            else if ($result->status == '2')
                $status = $result->driver_fname . ' accepted.';
            else if ($result->status == '3')
                $status = $result->driver_fname . ' rejected.';
            else if ($result->status == '4')
                $status = 'Passenger has cancelled.';
            else if ($result->status == '5')
                $status = $result->driver_fname . ' ' . $result->driver_lname . ' has canceled';
            else if ($result->status == '6')
                $status = $result->driver_fname . ' on the way.';
            else if ($result->status == '7')
                $status = 'Appointment started.';
            else if ($result->status == '8')
                $status = $result->driver_fname . ' Arrived';
            else if ($result->status == '9')
                $status = 'Appointment completed.';
            else if ($result->status == '10')
                $status = 'Appointment Timed out.';
            else
                $status = 'Status unavailable.';

            $code .= '<td class="v-align-middle"  style="padding: 7px!important;font-size: 2.5px;">'.$status.'</td></tr>';


            $arraytosend[]=array(

                'slno' => $slno,
                 'appointment_id' => $result->appointment_id,
                  'p_name' => $result->pessanger_fname.$result->pessanger_lname,
                'phone' => $result->phone,
                'app_dt' =>date("M d Y g:i A", strtotime($result->appointment_dt)),
                'address_pick' => $result->address_line1.$result->address_line2,
                'drop_address' => $result->drop_addr1.$result->drop_addr2,
                  'status' => $status
            );

            $slno++;

        }




        $queryforreview =  $this->db->query("select r.review_dt,r.slave_id,r.review,r.appointment_id,r.star_rating,s.first_name,s.email,s.slave_id from master_ratings r,slave s where r.mas_id='".$driverid."' and s.slave_id = r.slave_id")->result();



        $slno = 1;

        $review = '';
        foreach ($queryforreview as $result) {


//             $review = array($slno,$result->appointment_id, $result->first_name, $result->review,date("M d Y g:i A", strtotime($result->review_dt)),$result->star_rating);

            $review .= '<tr role="row"  class="gradeA odd">
                <td id = "d_no" class="v-align-middle sorting_1" style="padding: 7px!important;font-size: 2.5px;"> <p> '.$slno.'</p></td>
                <td id = "d_no" class="v-align-middle sorting_1" style="padding: 7px!important;font-size: 2.5px;"> <p>'.$result->appointment_id.'</p></td>
                <td class="v-align-middle" style="padding: 7px!important;font-size: 2.5px;"> '.$result->first_name.'</td>
                <td class="v-align-middle" style="padding: 7px!important;font-size: 2.5px;"> '.$result->review.'</td>
                <td class="v-align-middle" style="padding: 7px!important;font-size: 2.5px;"> '.date("M d Y g:i A", strtotime($result->review_dt)).'</td>
                <td class="v-align-middle" style="padding: 7px!important;font-size: 2.5px;">'.$result->star_rating.'</td></tr>';


            $arraytosend2[]=array(

                'slno' => $slno,
                'appointment_id' => $result->appointment_id,
                'D_name' => $result->first_name,
                'review' => $result->review,
                'app_dt' =>date("M d Y g:i A", strtotime($result->appointment_dt)),
                'star' => $result->star_rating,
            );

            $slno++;
        }















         $queryforprofile = $this->db->query("select * from master where mas_id='".$driverid."' ")->result();

      $profile = 'no data';

        foreach ($queryforprofile as $result) {

            if($result->profile_pic)
                $img = base_url()."../pics/".$result->profile_pic;
            else
                $img = "http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif";

            $profile = '<div class="row">
   <div class="col-md-12">
      <div class="panel-body">
         <div class="row">
            <div class="col-sm-12">

                <form id="form-work" class="form-horizontal" method="post" role="form" autocomplete="off" novalidate="novalidate" action="udpadedataProfile">
                  <div class="col-sm-4">
                     <center>
                        <div class="col_2">
                           <div class="img" id="prof_img">
                              <img style="-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);width: 126px;height: 127px;" class="img-circle" id="uimg" src='.$img.' enctype="multipart/form-data">
                           </div>
                           <div class="img" id="prof_img" style="margin-top: 28px;">
                              </button>
                           </div>
                        </div>
                     </center>
                  </div>
                  <div class="col-sm-8">
                     <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">First Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control"   value="'.$result->first_name.'"  aria-required="true" disabled>

                        </div>
                     </div>
                     <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Last Name</label>
                        <div class="col-sm-9">
                         <input type="text" class="form-control"   value="'.$result->last_name.'"  aria-required="true" disabled>

                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                  </div>
                  <div class="col-sm-12">
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="position" class="col-sm-3 control-label">Mobile</label>
                           <div class="col-sm-9">
                                  <input type="text" class="form-control"   value="'.$result->mobile.'"  aria-required="true" disabled>

                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="name" class="col-sm-3 control-label">Email</label>
                           <div class="col-sm-9">
                            <input type="text" class="form-control"  value="'.$result->email.'"  aria-required="true" disabled>

                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="name" class="col-sm-3 control-label">TAX NUMBER</label>
                           <div class="col-sm-9">
                            <input type="text" class="form-control"  value="'.$result->tax_num.'"  aria-required="true" disabled>

                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                  </div>
                  <div class="col-sm-12">

                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="name" class="col-sm-3 control-label">License Number </label>
                           <div class="col-sm-9">
                            <input type="text" class="form-control"  value= "'.$result->license_num.'"  aria-required="true" disabled>

                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="name" class="col-sm-3 control-label">License Type</label>
                           <div class="col-sm-9">
                            <input type="text" class="form-control"   value="'.$result->license_type.'"  aria-required="true" disabled>

                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="name" class="col-sm-3 control-label">License expiry date </label>
                           <div class="col-sm-9">
                            <input type="text" class="form-control"   value="'.$result->license_exp.'"  aria-required="true" disabled>

                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                  </div>
            </form>
            </div>
         </div>
      </div>
   </div>
</div>';
        }

                $this->load->library('mongo_db');

        $db = $this->mongo_db->db;
//        $location = $db->selectCollection('location');

//        $this->load->library('mongo_db');
//        $get_where = $this->mongo_db->get_one('Networks',array('_id' => new MongoId($id)));


        $collection = $db->location;

        $Query = array('user' => (int)$driverid); //,'status' => 3

        $latlong = $collection->findOne($Query);





        echo json_encode(array('jobs' => $arraytosend ,'test' => $code,'rest' => $succ,'profile' => $profile,'review' => $review,'reviewtable'=>$arraytosend2,'lat' =>$latlong['location']['latitude'],'long' => $latlong['location']['longitude'],'test1' =>"select r.review_dt,r.slave_id,r.review,r.appointment_id,r.star_rating,s.first_name,s.email,s.slave_id from master_ratings r,slave s where r.mas_id='".$driverid."' and s.slave_id = r.slave_id"));


    }

    function setsessiondata($tablename, $LoginId, $res,$email,$password) {
        $sessiondata = array(
            'emailid' => $email,
            'password' => $password,
            'LoginId' => $res->$LoginId,
            'first_name' => $res->dis_name,
            'table' => $tablename,
            'validate' => true
        );
        return $sessiondata;
    }

    function  get_appointment_details(){



       $today = date('Y-m-d');//appointment_dt like '".$today."%'


        $query = $this->db->query("select (select type_name from workplace_types where type_id = a.type_id)as vt_name,a.appointment_id,a.complete_dt,a.appointment_dt,a.B_type,a.address_line1,a.address_line2,a.apprxAmt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,p.first_name as pessanger_fname,p.last_name as pessanger_lname,p.phone,a.status,(select first_name from master where mas_id = a.mas_id) as first_name,(select mobile from master where mas_id = a.mas_id) as mobile from appointment a,slave p where  a.slave_id=p.slave_id  and a.appt_type = 2  and a.appointment_id ='".$this->input->post('app_id')."' ")->result();
//        echo $query;

//        $sendInfo = array();
//        foreach($query as $result){
//
//            $sendInfo['appointment_id']
//
//        }
        echo json_encode(array('data' => $query));


    }

      function   get_app_bookings(){
          $currTime = time();
          $today = date('Y-m-d H:S:I', $currTime);
          $query = $this->db->query("select (select type_name from workplace_types where type_id = a.type_id)as vt_name,a.appointment_id,a.complete_dt,a.appointment_dt,a.B_type,a.address_line1,a.address_line2,a.apprxAmt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,p.first_name as pessanger_fname,p.last_name as pessanger_lname,p.phone,a.status from appointment a,slave p where a.slave_id=p.slave_id  and a.status = 1 and a.appt_type = 2 and a.appointment_dt >= '".$today."' order by a.appointment_id desc")->result();
         return $query;
        }

    function  get_ongoing_bookings(){
        $currTime = time();
        $today = date('Y-m-d', $currTime);
        $query = $this->db->query("select m.first_name,m.last_name,a.mas_id,m.mobile as dphone,a.appointment_id,a.complete_dt,a.appointment_dt,a.B_type,a.address_line1,a.address_line2,a.apprxAmt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,p.first_name as pessanger_fname,p.last_name as pessanger_lname,p.phone,a.status from appointment a,slave p,master m where a.mas_id = m.mas_id and a.slave_id=p.slave_id  and a.status IN (6,7,8) and a.appt_type = 2 order by a.appointment_id desc LIMIT 200")->result();
        return $query;
    }

    function   get_app_bookings_history($status = ''){

        $query = $this->db->query("select a.appointment_id,a.complete_dt,a.appointment_dt,a.B_type,a.address_line1,a.address_line2,a.apprxAmt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,p.first_name as pessanger_fname,p.last_name as pessanger_lname,p.phone,a.status from appointment a,slave p where a.slave_id=p.slave_id  and  a.appt_type = 2 and a.status IN($status) order by a.appointment_id desc LIMIT 200")->result();

        return $query;
    }

    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');
        $this->db->update($databasename, $formdataarray, array($db_field_id_name => $IdToChange));
    }

    function LoadAdminList() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->find(array('Role' => "SubAdmin"));
        $db->close();
        return $cursor;
    }

    function getuserinfo() {
        $query = $this->db->query("SELECT * FROM slave where slave_id='" . $this->session->userdata("LoginId") . "'")->row();
        return $query;
    }

    function AddNewAdmin() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;

        $document = array(
            "Fname" => $this->input->post("Firstname"),
            "Lname" => $this->input->post("Lastname"),
            "Email" => $this->input->post("Email"),
            "Password" => md5($this->input->post("Password")),
            "Role" => "SubAdmin",
            "Parent" => "SuperAdmin",
            "Last_Login_Time" => NULL,
            "Last_Login_Ip" => NULL,
            "resetlink" => NULL
        );
        $collection->insert($document);

        $template = "<h3>you are added as SubAdmin  here is your login details</h3><br>"
            . "Emailid: " . $this->input->post("Email") . "<br>" .
            "Password: " . $this->input->post("Password") . "<br>";
        $to[] = array(
            'email' => $this->input->post("Email"),
            'name' => "prakash",
            'type' => "to");

        $from = "prakashjoshi9090@gmail.com";

        $subject = "Login Details";

        $this->sendMail($template, $to, $from, $subject);
        $db->close();
    }

    function ChangePassword($NewPassword, $EmailId) {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $collection->update(array("Email" => $EmailId), array('$set' => array("Password" => md5($NewPassword))), array("multiple" => true));
        $this->session->set_userdata('password', $NewPassword);
        $db->close();
    }

    function ForgotPassword($useremail) {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;

        $cursor = $collection->findOne(array('Email' => $useremail));

        if ($cursor) {
            $rlink = md5(mt_rand());
            $resetlink = base_url() . "index.php/superadmin/VerifyResetLink/" . $rlink;
            $template = "<h3> Click below link to reset your password</h3><br>" . $resetlink;
            $to[] = array(
                'email' => $useremail,
                'name' => "prakash",
                'type' => "to");

            $from = "prakashjoshi9090@gmail.com";
            $subject = "Reset Password Link";
            $this->sendMail($template, $to, $from, $subject);
            $collection->update(array("Email" => $useremail), array('$set' => array("resetlink" => ($rlink))), array("multiple" => true));

            $db->close();

            return true;
        }
        return false;
    }

    function VerifyResetLink($vlink) {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->findOne(array('resetlink' => $vlink));

        if ($cursor) {
            $password = md5("joshi");
            $collection->update(array("resetlink" => $vlink), array('$set' => array("Password" => $password)), array("multiple" => true));


            return true;
        }
        return false;
    }

    function sendMail($template, $to, $from, $subject) {
        require("src/Mandrill.php");

        try {

            $mandrill = new Mandrill('sHHx9KbktCU4idl6iechig');
            $message = array(
                'html' => ($template),
                'text' => 'Example text content',
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => 'Ryland Insurence',
                'to' => $to,
                'headers' => array('Reply-To' => "prakashjoshi9090@gmail.com"),
                'important' => false,
                'track_opens' => null,
                'track_clicks' => null,
                'auto_text' => null,
                'auto_html' => null,
                'inline_css' => null,
                'url_strip_qs' => null,
                'preserve_recipients' => null,
                'view_content_link' => null,
                'bcc_address' => 'message.bcc_address@example.com',
                'tracking_domain' => null,
                'signing_domain' => null,
                'return_path_domain' => null,
                'merge' => true,
                'merge_language' => 'mailchimp',
                'metadata' => array('website' => 'www.RylandIncurence.com'),
            );

            $async = false;
            $ip_pool = 'Main Pool';
            $result = $mandrill->messages->send($message, $async, $ip_pool);
            $result['flag'] = 0;
            $result['message'] = $message;


            return true;
        } catch (Mandrill_Error $e) {
            return false;
        }
    }

    function get_add_dreivers (){

        $this->load->library('mongo_db');


//        $get_where = $this->mongo_db->get_where('location',array('status' => 3));
        $db= $this->mongo_db->db;
//        $collection = $db->location;
//        if($this->input->post('status') == 3)
//        $get_where = $collection->find(array('loggedIn' => 1 ));
//        else if($this->input->post('status') == 4)
//            $get_where = $collection->find(array('$or' => array(array('loggedIn' => 2),array('status' => 4))));
//        $onlinedrivers = array();
//        foreach($get_where as $result){
//            $onlinedrivers[]=$result['user'];
//        }
//
//        if($this->input->post('status') == 3 ||$this->input->post('status') == 4 ) {
//            if (!empty($onlinedrivers))
//                $query = $this->db->query("SELECT m.mas_id ,m.profile_pic,m.first_name,m.last_name,m.mobile FROM  master m WHERE m.mas_id IN (" . implode(',', $onlinedrivers) . ")")->result();
//        }
//        else{
//            $query = $this->db->query("SELECT DISTINCT m.* FROM  master m,user_sessions us WHERE  m.mas_id = us.oid and m.status NOT IN(1,2,4)")->result();
//        }




           $status = $this->input->post('status');

        $collection = $db->location;
        if ($status == '4')// offline
            $user_data = $collection->find(array('status' => 4));
        else if ($status == '3') // online
            $user_data = $collection->find(array('status' => 3));


        $user_query = '';

        foreach ($user_data as $driver) {
            $user_query .= $driver['user'] . ',';
        }
        $accQry = '';

        if($status == '4' || $status == '3' ) {


            $accQry = "SELECT m.mas_id,m.first_name,m.last_name,m.profile_pic,m.mobile,m.company_id,m.last_active_dt, m.workplace_id,m.type_id,(select uniq_identity from workplace where workplace_id = m.workplace_id) as uniq_identity,(select type_name from workplace_types where type_id = m.type_id) as vehicletype,(select companyname from company_info where company_id=m.company_id) as companyname FROM master m WHERE m.mas_id IN (" . rtrim($user_query, ',') . ") ORDER BY m.last_active_dt DESC ";

        }else if($status == '2'){
            $typeQry = ",(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type ";

            $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas where  mas.status IN (3)  order by mas.mas_id DESC";
        }







        $query = $this->db->query($accQry)->result();




        $slno = 1;
        $code = '';
        $arraytosend = array();
        foreach ($query as $result) {
//            $code .= '<tr role="row"  class="gradeA odd">
//                <td id = "d_no" class="v-align-middle sorting_1" style="padding: 8px !important;"> <p style="float: left;">
//                        <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#" onclick="get_driver_data("'.$result->oid.'")" >
//                                                                                            <span class="col-xs-height col-middle">
//                                                                                            <span class="thumbnail-wrapper d32 circular bg-success" >';
//                                                                                                if($result->profile_pic)
//                                                                                                    $code .='<img width="34" height="34" alt="" data-src-retina="http://107.170.66.211/roadyo_live/pics/'.$result->profile_pic.'" data-src="http://107.170.66.211/roadyo_live/pics/'.$result->profile_pic.'" src="http://107.170.66.211/roadyo_live/pics/'.$result->profile_pic.'" class="col-top">';
//                                                                                                else
//                                                                                                    $code .='<img width="34" height="34" alt=""  src="http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif" class="col-top">';
//                         $code .= '</span></span><p class="p-l-10 col-xs-height col-middle" style="">
//                                <span class="text-master"> '.$result->first_name.$result->last_name.'</span>
//                                <span class="block text-master hint-text fs-12">'.$result->mobile.'</span>
//                            </p></a></p></td></tr>';
//
//
//            $slno++;

            if($result->profile_pic)
                $img = base_url()."../pics/hdpi/".$result->profile_pic;
                else
                $img = "http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif";

            $code .= '<div class="domstatus" alt="'.(int)$this->input->post('status').'"><p style="float: left;" class="u'.$result->mas_id.'">
                        <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#" onclick="get_driver_data('.$result->mas_id.')" >
                                                                                            <span class="col-xs-height col-middle">
                                                                                            <span class="thumbnail-wrapper d32 circular bg-success" >';

                $code .='<img width="34" height="34" alt="" data-src-retina="'.$img.'" data-src="'.$img.'" src="'.$img.'" class="col-top">';

            $code .= '</span></span><p class="p-l-10 col-xs-height col-middle" style="">
                                <span class="text-master"> '.$result->first_name.'</span>
                                <span class="block text-master hint-text fs-12">'.$result->mobile.'</span>
                            </p></a></p></div>';


            $slno++;
            $arraytosend[] = array(
                "data" =>$code,
            );
            $code = '';
        }
        //
        echo json_encode(array('html' => $arraytosend,'user' =>"SELECT us.oid,m.profile_pic,m.first_name,m.last_name,m.mobile FROM user_sessions us, master m WHERE us.oid = m.mas_id and m.mas_id IN ('".implode(',',$onlinedrivers)."')"));

    }



    function issessionset() {

        if ($this->session->userdata('emailid') && $this->session->userdata('password')) {

            return true;
        }
        return false;
    }

}

?>
