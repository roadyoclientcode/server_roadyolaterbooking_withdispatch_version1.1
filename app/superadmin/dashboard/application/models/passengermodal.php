<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

class Passengermodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
    }

    function validateSuperAdmin() {

        $email = $this->input->post("email");
        $password = $this->input->post("password");

        $queryforslave = $this->db->get_where('slave', array('email' => $email, 'password' => md5($password)));
        $res = $queryforslave->row();


        if ($queryforslave->num_rows > 0) {
            $tablename = 'slave';
            $LoginId = 'slave_id';
            $sessiondata = $this->setsessiondata($tablename, $LoginId, $res, $email, $password);
            $this->session->set_userdata($sessiondata);
            return true;
        }

        return false;
    }

    function setsessiondata($tablename, $LoginId, $res, $email, $password) {
        $sessiondata = array(
            'emailid' => $email,
            'password' => $password,
            'LoginId' => $res->$LoginId,
            'profile_pic' => $res->profile_pic,
            'first_name' => $res->first_name,
            'last_name' => $res->last_name,
            'table' => $tablename,
            'validate' => true
        );
        return $sessiondata;
    }

    function getPassangerBooking() {
        $query = $this->db->query("select a.appointment_id,a.complete_dt,a.amount,a.inv_id,a.distance_in_mts,a.appointment_dt,"
                        . "a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,d.first_name as doc_firstname,d.profile_pic as doc_profile,"
                        . "d.last_name as doc_lastname,p.first_name as patient_firstname,p.last_name as patient_lastname,"
                        . "a.address_line1,a.address_line2,a.status from appointment a,master d,"
                        . "slave p where a.slave_id=p.slave_id and d.mas_id = a.mas_id and a.status = 9 and a.slave_id='" . $this->session->userdata("LoginId") . "' order by a.appointment_id desc")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }
    function upload_images_on_local() {
       $file_formats = array("jpg", "png", "gif", "jpeg");

$filepath = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';


$name = $_FILES['myfile']['name']; // filename to get file's extension
$size = $_FILES['myfile']['size'];



if (!strlen($name)) {
    echo "Please select image..!";
    return false;
}

$ext = substr($name, strrpos($name, '.') + 1);
if (!in_array($ext, $file_formats)) {
    echo "Invalid file format.";
    return false;
}// check it if it's a valid format or not
if ($size > (2048 * 1024)) { // check it if it's bigger than 2 mb or no
    echo "Your image size is bigger than 2MB.";
    return FALSE;
}

$imagename = md5(uniqid() . time()) . "." . $ext;
$file_to_open = $filepath . $imagename;
$tmp1 = $_FILES['myfile']['tmp_name'];


try{

    $move = move_uploaded_file($tmp1, $file_to_open);
//    if (!$move) {
//
//        echo "Could not move the file.".$_SERVER['DOCUMENT_ROOT'].'/guyidee/pics';
//        return false;
//    }
}catch (Exception $ex){

    print_r($ex);
    return false;
}


//if ($_REQUEST['image_type'] == 1) {
//
//    $updateDoctorPicQry = "update master set profile_pic = '" . $imagename . "' where mas_id = '" . $_SESSION['admin_id'] . "'";
//    mysql_query($updateDoctorPicQry, $db->conn);
//
//    $location = $db->mongo->selectCollection('location');
//
//    $newdata = array('$set' => array("image" => $imagename));
//    $location->update(array("user" => (int) $_SESSION['admin_id']), $newdata);
//
//    $pic_width = 215;
//    $pic_height = 150;
//    $image_title = "Main profile picture";
//    $disPic = '../pics/xxhdpi/' . $imagename;
//    $echo = "<img id='updatedimagecss'  src=\"" . $disPic . "\" border=\"0\" title=\"$image_title\" width=\"" . $pic_width . "\" height=\"" . $pic_height . "\"/><script>$('.click_to_crop').trigger('click');</script>";
//    ;
//} else {
//
//    $pic_width = 100;
//    $pic_height = 70;
//    $image_title = "Other profile images";
//
//    $insert = "insert into images(doc_id,image)values('" . $_SESSION['admin_id'] . "','" . $imagename . "')";
//    mysql_query($insert, $db->conn);
//    $max_id = mysql_insert_id();
//
//    $id_attr = "p_id='$max_id'";
//    $disPic = '../pics/xxhdpi/' . $imagename;
//
//    $echo = '<div style="position:relative;">
//                                        <img ' . $id_attr . ' src="' . $disPic . '" border="0" width="' . $pic_width . '" height= "' . $pic_height . '"/>
//                                        <div style="position:absolute;top:0;right:0px;">
//                                            <img p_flag="0" p_id="' . $max_id . '" p_th_num="' . $max_id . '" p_src="' . $file_to_open . '" class="thumb_image" src="assets/dialog_close_button.png" height="20px" width="20px" />
//                                        </div>
//                                    </div>';
//}


list($width, $height) = getimagesize($file_to_open);

$ratio = $height / $width;



/* mdpi 36*36 */
$mdpi_nw = 36;
$mdpi_nh = $ratio * 36;

$mtmp = imagecreatetruecolor($mdpi_nw, $mdpi_nh);

if ($ext == "jpg" || $ext == "jpeg") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($mtmp, $new_image, 0, 0, 0, 0, $mdpi_nw, $mdpi_nh, $width, $height);

$mdpi_file = $servername.'pics/mdpi/' . $imagename;

imagejpeg($mtmp, $mdpi_file, 100);

/* HDPI Image creation 55*55 */
$hdpi_nw = 55;
$hdpi_nh = $ratio * 55;

$tmp = imagecreatetruecolor($hdpi_nw, $hdpi_nh);

if ($ext == "jpg" || $ext == "jpeg") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($tmp, $new_image, 0, 0, 0, 0, $hdpi_nw, $hdpi_nh, $width, $height);

$hdpi_file = $servername.'pics/hdpi/' . $imagename;

imagejpeg($tmp, $hdpi_file, 100);

/* XHDPI 84*84 */
$xhdpi_nw = 84;
$xhdpi_nh = $ratio * 84;

$xtmp = imagecreatetruecolor($xhdpi_nw, $xhdpi_nh);

if ($ext == "jpg" || $ext == "jpeg") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($xtmp, $new_image, 0, 0, 0, 0, $xhdpi_nw, $xhdpi_nh, $width, $height);

$xhdpi_file = $servername.'pics/xhdpi/' . $imagename;

imagejpeg($xtmp, $xhdpi_file, 100);

/* xXHDPI 125*125 */
$xxhdpi_nw = 125;
$xxhdpi_nh = $ratio * 125;

$xxtmp = imagecreatetruecolor($xxhdpi_nw, $xxhdpi_nh);

if ($ext == "jpg" || $ext == "jpeg") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($xxtmp, $new_image, 0, 0, 0, 0, $xxhdpi_nw, $xxhdpi_nh, $width, $height);

$xxhdpi_file = $servername.'pics/xxhdpi/' . $imagename;

imagejpeg($xxtmp, $xxhdpi_file, 100);


echo json_encode(array('msg' => '1', 'fileName' => $imagename));
    }

    function getbooking_data() {

        $this->load->library('Datatables');
        $this->load->library('table');
//        return json_encode(array('test' => 'rtest'));

        $query = "a.slave_id = s.slave_id and a.slave_id = '" . $this->session->userdata("LoginId") . "' and a.mas_id = m.mas_id and a.status = 9";
        $this->datatables->select("a.appointment_id,m.first_name,m.profile_pic,a.address_line1,a.drop_addr1,DATE_FORMAT(a.appointment_dt,'%b %d %Y %h:%i %p'),DATE_FORMAT(a.complete_dt,'%b %d %Y %h:%i %p'),a.amount,a.type_id", false)
//                ->unset_column('m.profile_pic')
                ->edit_column('m.profile_pic', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'm.profile_pic')
                ->edit_column('a.type_id', '<a target="_blank" href="' . base_url() . '../../getPDF.php?apntId=$1"><button type="button" name="INVOICE"  width="50px">INVOICE</button></a>', 'a.appointment_id')
                ->from('appointment a,master m,slave s')
                ->where($query);

        echo $this->datatables->generate();
    }

    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');


        $this->db->update('slave', $formdataarray, array('slave_id' => $IdToChange));

        $this->session->set_userdata(array('profile_pic' => $formdataarray['profile_pic'],
            'first_name' => $formdataarray['first_name'],
            'last_name' => $formdataarray['last_name']));
    }

    function LoadAdminList() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->find(array('Role' => "SubAdmin"));
        $db->close();
        return $cursor;
    }

    function changeslavepassword() {


        $newpassword = $this->input->post('newpass');


        $data = $this->db->query("select password from slave where slave_id ='" . $this->session->userdata("LoginId") . "'")->row_array();

        if ($data['password'] == md5($newpassword)) {

            return json_encode(array('msg' => "you have entered the same password!, please enter the new password", "flag" => 1));
        } else {
            $this->db->update('slave', array('password' => md5($newpassword)), array('slave_id' => $this->session->userdata("LoginId")));

            return json_encode(array('msg' => "your new password updated successfully", "flag" => 0));
        }
    }

    function getuserinfo() {
        $query = $this->db->query("SELECT * FROM slave where slave_id='" . $this->session->userdata("LoginId") . "'")->row();
        return $query;
    }

    function AddNewAdmin() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;

        $document = array(
            "Fname" => $this->input->post("Firstname"),
            "Lname" => $this->input->post("Lastname"),
            "Email" => $this->input->post("Email"),
            "Password" => md5($this->input->post("Password")),
            "Role" => "SubAdmin",
            "Parent" => "SuperAdmin",
            "Last_Login_Time" => NULL,
            "Last_Login_Ip" => NULL,
            "resetlink" => NULL
        );
        $collection->insert($document);

        $template = "<h3>you are added as SubAdmin  here is your login details</h3><br>"
                . "Emailid: " . $this->input->post("Email") . "<br>" .
                "Password: " . $this->input->post("Password") . "<br>";
        $to[] = array(
            'email' => $this->input->post("Email"),
            'name' => "prakash",
            'type' => "to");

        $from = "prakashjoshi9090@gmail.com";

        $subject = "Login Details";

        $this->sendMail($template, $to, $from, $subject);
        $db->close();
    }

    function ChangePassword($NewPassword, $EmailId) {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $collection->update(array("Email" => $EmailId), array('$set' => array("Password" => md5($NewPassword))), array("multiple" => true));
        $this->session->set_userdata('password', $NewPassword);
        $db->close();
    }

    function ForgotPassword($useremail) {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;

        $cursor = $collection->findOne(array('Email' => $useremail));

        if ($cursor) {
            $rlink = md5(mt_rand());
            $resetlink = base_url() . "index.php/superadmin/VerifyResetLink/" . $rlink;
            $template = "<h3> Click below link to reset your password</h3><br>" . $resetlink;
            $to[] = array(
                'email' => $useremail,
                'name' => "prakash",
                'type' => "to");

            $from = "prakashjoshi9090@gmail.com";
            $subject = "Reset Password Link";
            $this->sendMail($template, $to, $from, $subject);
            $collection->update(array("Email" => $useremail), array('$set' => array("resetlink" => ($rlink))), array("multiple" => true));

            $db->close();

            return true;
        }
        return false;
    }

    function VerifyResetLink($vlink) {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->findOne(array('resetlink' => $vlink));

        if ($cursor) {
            $password = md5("joshi");
            $collection->update(array("resetlink" => $vlink), array('$set' => array("Password" => $password)), array("multiple" => true));


            return true;
        }
        return false;
    }

    function sendMail($template, $to, $from, $subject) {
        require("src/Mandrill.php");

        try {

            $mandrill = new Mandrill('sHHx9KbktCU4idl6iechig');
            $message = array(
                'html' => ($template),
                'text' => 'Example text content',
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => 'Ryland Insurence',
                'to' => $to,
                'headers' => array('Reply-To' => "prakashjoshi9090@gmail.com"),
                'important' => false,
                'track_opens' => null,
                'track_clicks' => null,
                'auto_text' => null,
                'auto_html' => null,
                'inline_css' => null,
                'url_strip_qs' => null,
                'preserve_recipients' => null,
                'view_content_link' => null,
                'bcc_address' => 'message.bcc_address@example.com',
                'tracking_domain' => null,
                'signing_domain' => null,
                'return_path_domain' => null,
                'merge' => true,
                'merge_language' => 'mailchimp',
                'metadata' => array('website' => 'www.RylandIncurence.com'),
            );

            $async = false;
            $ip_pool = 'Main Pool';
            $result = $mandrill->messages->send($message, $async, $ip_pool);
            $result['flag'] = 0;
            $result['message'] = $message;


            return true;
        } catch (Mandrill_Error $e) {
            return false;
        }
    }

    function issessionset() {

        if ($this->session->userdata('emailid') && $this->session->userdata('password')) {

            return true;
        }
        return false;
    }

}

?>
