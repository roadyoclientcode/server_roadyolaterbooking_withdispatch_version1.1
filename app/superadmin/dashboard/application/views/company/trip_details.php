<script>
    $('.sm-table').find('.header-inner').html('<div class="brand inline" style="  width: auto;\
                     font-size: 27px;\
                     color: gray;\
                     margin-left: 100px;margin-right: 20px;margin-bottom: 12px; margin-top: 10px;">\
                    <strong>Roadyo Super Admin Console</strong>\
                </div>');
    $('document').ready(function () {
        var initStripedTable = function () {
            var table = $('.stripedTable');
            var settings = {
                "sDom": "t",
                "destroy": true,
                "paging": false,
                "scrollCollapse": true
            };
            table.dataTable(settings);
        }
        initStripedTable();
    });
</script>
<!--<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK5bz3K1Ns_BASkAcZFLAI_oivKKo98VE" type="text/javascript"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK5bz3K1Ns_BASkAcZFLAI_oivKKo98VE"></script>

<style>
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
</style>
<?php
$i = 0;
     $routes = array();
     foreach ($data['res']['route'] as $trip_data)
     {
        $routes[$j++] = array('Geometry' => array('Latitude' => $val->latitude, 'Longitude' => $val->longitude));
        $i++;
     }

    
$start = $routes[0];

$startLat = $start[1];
$startLong = $start[2];

$end = end($routes);

$endLat = $end[1];
$endLong = $end[2];

//$startLat = $data['appt_data']->appt_lat;
//$startLong = $data['appt_data']->appt_long;
//$i = 1;
////    print_r($data);die;
//$routes = array();
//$s_e_ship = array();
////     $routes[] = array('','12.839939','77.677003',0);
////     $routes[] = array('','12.922637','77.617444',0);
////     $routes[] = array('','13.019568','77.596813',0);
////     $routes[] = array('','12.990058','77.552492',0);
//    foreach ($data['res']['receivers'] as $trip_data)
//    {
//       $routes[] = array('', $trip_data['location']['lat'], $trip_data['location']['log'], $i);
//       $i++;
//    }
////$start = $routes[0];
$j = 0;
foreach ($data['trip_route'] as $trip_data) {
    $flg = true;
    foreach ($trip_data as $val) {
        if ($flg) {
            $last = end($s_e_ship);
            if($routes[$last]['Geometry']['Latitude'] != $val->latitude && $routes[$last]['Geometry']['Longitude'] != $val->longitude)
                $s_e_ship[] = $j;
            $flg = false;
        }
        $routes[$j++] = array('Geometry' => array('Latitude' => $val->latitude, 'Longitude' => $val->longitude));
        $i++;
    }
    $s_e_ship[] = $j - 1;
}
$end = end($routes);

//    $startLat = $routes[0][1];
//    $startLong = $routes[0][2];

$endLat = $end[1];
$endLong = $end[2];
//    print_r($routes);die;
?>    
<!--<div class="header nav nav-tabs  bg-white" style="margin-left: 7%;">
    <h3 style="color:dimgrey;margin-top:2%">JOB DETAILS</h3>
</div>-->
<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?= base_url() ?>/index.php/superadmin/completed_jobs">Completed Jobs</a>
            </li>
            <li>
                <a href="#" class="active">Job Details - <?php echo $data['appt_data']->appointment_id; ?></a>
            </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Job Details
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                    <li class="active">
                                        <a href="#customer_details" data-toggle="tab" role="tab" aria-expanded="true">Customer Details</a>
                                    </li>
                                    <li class="">
                                        <a href="#driver_details" data-toggle="tab" role="tab" aria-expanded="false">Driver Details</a>
                                    </li>
                                </ul><div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                                <div class="tab-content hidden-xs">
                                    <div class="tab-pane active" id="customer_details">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking ID</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->appointment_id; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Type</a>
                                                </p>
                                                <p class="small">
                                                    <?php if ($data['appt_data']->appt_type == 1)
                                                        echo "Now";
                                                    else
                                                        "Later";
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Time</a>
                                                </p>
                                                <p class="small">
    <?php echo date('g:i A', strtotime($data['appt_data']->created_dt)); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Customer</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['customer_data']->first_name; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Phone</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['customer_data']->phone; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Email</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['customer_data']->email; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Pickup Address</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->address_line1; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Pickup Time</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->start_dt; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Drop Address</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->drop_addr1; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Drop Time</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->complete_dt; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Waiting Time</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->waiting_mts; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Time</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->start_dt; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Distance</a>
                                                </p>
                                                <p class="small">
    <?php 
        if($data['appt_data']->distance_in_mts != NULL)
        {    

            echo bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2); 
             echo " Miles";
        }
    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="driver_details">
                                        <div class="row-xs-height">
                                            <div class="social-user-profile col-xs-height text-center col-top">
                                                <div class="thumbnail-wrapper circular bordered b-white">
                                                    <img alt="<?php echo $data['driver_data']->first_name; ?>" width="55" height="55" style="cursor:pointer;"
                                                         onclick='openimg(this)'
                                                         data-src-retina="<?php echo $data['driver_data']->profile_pic ?>" 
                                                         data-src="<?php echo $data['driver_data']->profile_pic ?>"
                                                         src="<?php echo $data['driver_data']->profile_pic ?>"
                                                         onerror="this.src = '<?php echo base_url('/../../pics/user.jpg') ?>'">
                                                </div>
                                                <p class="rating">
                                                    <?php
                                                    for ($i = 1; $i <= 5; $i++) {
                                                        if ($i > $data['res']['rating'])
                                                            echo '<i class="fa fa-star"></i>';
                                                        else
                                                            echo '<i class="fa fa-star rated"></i>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-xs-height p-l-20">
                                                <h3><?php echo $data['driver_data']->first_name; ?></h3>
                                                <p class="fs-16"><?php echo $data['driver_data']->email; ?></p>
                                                <p class="no-margin fs-16"><?php echo $data['driver_data']->mobile; ?></p>
                                            </div>
                                        </div>
<!--                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Trip Distance</p>
                                                <p class="pull-right bold">
                                                    <?php 
                                                        if($data['appt_data']->distance_in_mts != NULL)
                                                        {    

                                                            echo bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2); 
                                                            echo " Miles";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Trip Duration</p>
                                                <p class="pull-right bold">
                                                    <?php echo gmdate("H:i:s",($data['appt_data']->duration * 60));?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Distance Fee</p>
                                                <p class="pull-right bold">
                                                    <?php print_r($data['car_data']->price_per_km * bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2)." $");?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Time Fee</p>
                                                <p class="pull-right bold">
                                                    <?php print_r($data['car_data']->price_per_min * $data['appt_data']->duration." $");?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Parking Fee</p>
                                                <p class="pull-right bold">
                                                    <?php echo $data['appt_data']->parking_fee." $";?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Subtotal</p>
                                                <p class="pull-right bold">
                                                    <?php echo ($data['appt_data']->amount-$data['appt_data']->discount)." $"; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Discounts</p>
                                                <p class="pull-right bold">
                                                    <?php echo $data['appt_data']->discount." $"; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Final Fare</p>
                                                <p class="pull-right bold">
                                                    <?php echo $data['appt_data']->amount," $"; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left">Payment Method</p>
                                                <p class="pull-right bold">
                                                    <?php if($data['appt_data']->payment_type == 1)echo 'Card';else echo 'Cash';?>
                                                </p>
                                            </div>
                                        </div>-->
                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Distance</a>
                                                </p>
                                                <p class="small">
                <?php 
                    if($data['appt_data']->distance_in_mts != NULL)
                    {    

                        echo bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2); 
                        echo " Miles";
                    }
                ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Duration</a>
                                                </p>
                                                <p class="small">
    <?php echo gmdate("H:i:s",($data['appt_data']->duration * 60));?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Distance Fee</a>
                                                </p>
                                                <p class="small">
    <?php print_r($data['car_data']->price_per_km * bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2)." $");?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Time Fee</a>
                                                </p>
                                                <p class="small">
    <?php print_r($data['car_data']->price_per_min * $data['appt_data']->duration." $");?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Parking Fee</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->parking_fee." $";?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Subtotal</a>
                                                </p>
                                                <p class="small">
    <?php echo ($data['appt_data']->amount-$data['appt_data']->discount)." $"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Discounts</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->discount." $"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Final Fare</a>
                                                </p>
                                                <p class="small">
    <?php echo $data['appt_data']->amount," $"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Payment Method</a>
                                                </p>
                                                <p class="small">
    <?php if($data['appt_data']->payment_type == 1)echo 'Card';else echo 'Cash';?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12">
                            <!--                            <div class="panel panel-transparent">
                                                            <div class="panel-body">-->
                            <div id="map" style="width: 100%; height: 670px;"></div>
                            <!--                                </div>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    jQuery(function () {
        //    var stops = [
        //                        {"Geometry":{"Latitude":52.1615470947258,"Longitude":20.80514430999756}},
        //                        {"Geometry":{"Latitude":52.15991486090931,"Longitude":20.804049968719482}},
        //                        {"Geometry":{"Latitude":52.15772967999426,"Longitude":20.805788040161133}},
        //                        {"Geometry":{"Latitude":52.15586034371232,"Longitude":20.80460786819458}},
        //                        {"Geometry":{"Latitude":52.15923693975469,"Longitude":20.80113172531128}},
        //                        {"Geometry":{"Latitude":52.159849043774074, "Longitude":20.791990756988525}},
        //                        {"Geometry":{"Latitude":52.15986220720892,"Longitude":20.790467262268066}},
        //                        {"Geometry":{"Latitude":52.16202095784738,"Longitude":20.7806396484375}},
        //                        {"Geometry":{"Latitude":52.16088894313116,"Longitude":20.77737808227539}},
        //                        {"Geometry":{"Latitude":52.15255590234335,"Longitude":20.784244537353516}},
        //                        {"Geometry":{"Latitude":52.14747369312591,"Longitude":20.791218280792236}},
        //                        {"Geometry":{"Latitude":52.14963304460396,"Longitude":20.79387903213501}}
        //
        //
        //
        //                    ] ;
        var stops = jQuery.parseJSON('<?php echo json_encode($routes); ?>');
        if (stops != "")
        {
        var map = new window.google.maps.Map(document.getElementById("map"));

        // new up complex objects before passing them around
            var directionsDisplay = new window.google.maps.DirectionsRenderer({suppressMarkers: true});
            var directionsService = new window.google.maps.DirectionsService();

            Tour_startUp(stops);

            window.tour.loadMap(map, directionsDisplay);
            window.tour.fitBounds(map);

            if (stops.length > 1)
            window.tour.calcRoute(directionsService, directionsDisplay);
        }
        else
        {
            var directionsDisplay;
                var directionsService = new google.maps.DirectionsService();
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(<?php echo json_encode($data['appt_data']->appt_lat); ?>, <?php echo json_encode($data['appt_data']->appt_long); ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            directionsDisplay = new google.maps.DirectionsRenderer();

            directionsDisplay.setMap(map);

            var start = new google.maps.LatLng(<?php echo json_encode($data['appt_data']->appt_lat); ?>, <?php echo json_encode($data['appt_data']->appt_long); ?>);
            var end = new google.maps.LatLng(<?php echo json_encode($data['appt_data']->drop_lat); ?>, <?php echo json_encode($data['appt_data']->drop_long); ?>);

            var request = {
                origin: start,
                destination: end,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
                directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
                }             });
         }
    });

                    function Tour_startUp(stops) {
        if (!window.tour)
                window.tour = {
    updateStops: function (newStops) {
                    stops = newStops;
        },
                // map: google map object
        // directionsDisplay: google directionsDisplay object (comes in empty)
 
        loadMap: function (map, directionsDisplay) {
                    var myOptions = {
                        zoom: 13,
                        center: new window.google.maps.LatLng(51.507937, -0.076188), // default to London
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map.setOptions(myOptions);
                    directionsDisplay.setMap(map);
                },
                fitBounds: function (map) {
                    var bounds = new window.google.maps.LatLngBounds();

                    // extend bounds for each record
                    jQuery.each(stops, function (key, val) {
                        var myLatlng = new window.google.maps.LatLng(val.Geometry.Latitude, val.Geometry.Longitude);
                        bounds.extend(myLatlng);
                    });
                    map.fitBounds(bounds);
                },
                calcRoute: function (directionsService, directionsDisplay) {
                    var batches = [];
                    var itemsPerBatch = 10; // google API max = 10 - 1 start, 1 stop, and 8 waypoints
                    var itemsCounter = 0;
                    var wayptsExist = stops.length > 0;

                    while (wayptsExist) {
                        var subBatch = [];
                        var subitemsCounter = 0;

                        for (var j = itemsCounter; j < stops.length; j++) {
                            subitemsCounter++;
                            subBatch.push({
                                location: new window.google.maps.LatLng(stops[j].Geometry.Latitude, stops[j].Geometry.Longitude),
                                stopover: true
                            });
                            if (subitemsCounter == itemsPerBatch)
                                break;
                        }

                        itemsCounter += subitemsCounter;
                        batches.push(subBatch);
                        wayptsExist = itemsCounter < stops.length;
                        // If it runs again there are still points. Minus 1 before continuing to
                        // start up with end of previous tour leg
                        itemsCounter--;
                    }

                    // now we should have a 2 dimensional array with a list of a list of waypoints
                    var combinedResults;
                    var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
                    var directionsResultsReturned = 0;

                    for (var k = 0; k < batches.length; k++) {
                        var lastIndex = batches[k].length - 1;
                        var start = batches[k][0].location;
                        var end = batches[k][lastIndex].location;

                        // trim first and last entry from array
                        var waypts = [];
                        waypts = batches[k];
                        waypts.splice(0, 1);
                        waypts.splice(waypts.length - 1, 1);

                        var request = {
                            origin: start,
                            destination: end,
                            waypoints: waypts,
                            travelMode: window.google.maps.TravelMode.WALKING
                        };
                        (function (kk) {
                            directionsService.route(request, function (result, status) {
                                if (status == window.google.maps.DirectionsStatus.OK) {

                                    var unsortedResult = {order: kk, result: result};
                                    unsortedResults.push(unsortedResult);

                                    directionsResultsReturned++;

                                    if (directionsResultsReturned == batches.length) // we've received all the results. put to map
                                    {
                                        // sort the returned values into their correct order
                                        unsortedResults.sort(function (a, b) {
                                            return parseFloat(a.order) - parseFloat(b.order);
                                        });
                                        var count = 0;
                                        for (var key in unsortedResults) {
                                            if (unsortedResults[key].result != null) {
                                                if (unsortedResults.hasOwnProperty(key)) {
                                                    if (count == 0) // first results. new up the combinedResults object
                                                        combinedResults = unsortedResults[key].result;
                                                    else {
                                                        // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete
                                                        // directionResults object, but enough to draw a path on the map, which is all I need
                                                        combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                        combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);

                                                        combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                        combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                                    }
                                                    count++;
                                                }
                                            }
                                        }
                                        directionsDisplay.setDirections(combinedResults);
                                        var legs = combinedResults.routes[0].legs;
                                        // alert(legs.length);
                                        for (var i = 0; i < legs.length; i++) {
//                                    console.log(legs[i]);
                                            var markerletter = "A".charCodeAt(0);
                                            markerletter += i;
                                            markerletter = String.fromCharCode(markerletter);
//                                  createMarker(directionsDisplay.getMap(),legs[i].start_location,"marker"+i,"some text for marker "+i+"<br>"+legs[i].start_address,markerletter);
                                        }
//                                        var shipments = jQuery.parseJSON('<?php echo json_encode($s_e_ship); ?>');
//                                        var msg = "Trip Started";
//                                        for (var i = 0; i < shipments.length - 1; i++) {
//                                            var markerletter = "A".charCodeAt(0);
//                                            markerletter += i;
//                                            markerletter = String.fromCharCode(markerletter);
//                                            createMarker(directionsDisplay.getMap(), legs[shipments[i]].start_location, msg, legs[i].start_address, markerletter);
//                                            msg = "Shipemnt " + (i+1);
//                                        }
//                                        var i = shipments.length - 1;
//                                        var markerletter = "A".charCodeAt(0);
//                                        markerletter += i;
//                                        markerletter = String.fromCharCode(markerletter);
//                                        createMarker(directionsDisplay.getMap(), legs[legs.length - 1].end_location, msg, legs[legs.length - 1].end_address, markerletter);
                                    }
                                }
                            });
                        })(k);
                    }
                }
            };
    }
    var infowindow = new google.maps.InfoWindow(
            {
                size: new google.maps.Size(150, 50)
            });

    var icons = new Array();
    icons["red"] = new google.maps.MarkerImage("mapIcons/marker_red.png",
            // This marker is 20 pixels wide by 34 pixels tall.
            new google.maps.Size(20, 34),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is at 9,34.
            new google.maps.Point(9, 34));



    function getMarkerImage(iconStr) {
        if ((typeof (iconStr) == "undefined") || (iconStr == null)) {
            iconStr = "red";
        }
        if (!icons[iconStr]) {
            icons[iconStr] = new google.maps.MarkerImage("http://www.google.com/mapfiles/marker" + iconStr + ".png",
                    // This marker is 20 pixels wide by 34 pixels tall.
                    new google.maps.Size(20, 34),
                    // The origin for this image is 0,0.
                    new google.maps.Point(0, 0),
                    // The anchor for this image is at 6,20.
                    new google.maps.Point(9, 34));
        }
        return icons[iconStr];

    }
    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.

    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.

    var iconImage = new google.maps.MarkerImage('mapIcons/marker_red.png',
            // This marker is 20 pixels wide by 34 pixels tall.
            new google.maps.Size(20, 34),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is at 9,34.
            new google.maps.Point(9, 34));
    var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(37, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(9, 34));
    // Shapes define the clickable region of the icon.
    // The type defines an HTML &lt;area&gt; element 'poly' which
    // traces out a polygon as a series of X,Y points. The final
    // coordinate closes the poly by connecting to the first
    // coordinate.
    var iconShape = {
        coord: [9, 0, 6, 1, 4, 2, 2, 4, 0, 8, 0, 12, 1, 14, 2, 16, 5, 19, 7, 23, 8, 26, 9, 30, 9, 34, 11, 34, 11, 30, 12, 26, 13, 24, 14, 21, 16, 18, 18, 16, 20, 12, 20, 8, 18, 4, 16, 2, 15, 1, 13, 0],
        type: 'poly'
    };


    function createMarker(map, latlng, label, html, color) {
// alert("createMarker("+latlng+","+label+","+html+","+color+")");
        var contentString = '<b>' + label + '</b><br>' + html;
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            shadow: iconShadow,
            icon: getMarkerImage(color),
            shape: iconShape,
            title: label,
            zIndex: Math.round(latlng.lat() * -100000) << 5
        });
        marker.myname = label;

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(contentString);
            infowindow.open(map, marker);
        });
        return marker;
    }
<!--/script>-->

//    var directionsDisplay;
//    var directionsService = new google.maps.DirectionsService();
//
//    if(locations != "")
//    {
//        console.log(locations);
//        
//        var map = new google.maps.Map(document.getElementById('map'), {
//            zoom: 10,
//            center: new google.maps.LatLng(<?php echo json_encode($startLat); ?>, <?php echo json_encode($startLong); ?>),
//            mapTypeId: google.maps.MapTypeId.ROADMAP
//        });
//        var infowindow = new google.maps.InfoWindow();
//        var marker, i;
//
//        for (i = 0; i < locations.length; i++) {
//            marker = new google.maps.Marker({
//                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//                map: map
//            });
//
//            google.maps.event.addListener(marker, 'click', (function (marker, i) {
//                return function () {
//                    infowindow.setContent(locations[i][1]);
//                    infowindow.open(map, marker);
//                }
//            })(marker, i));
//        }
//        
//        directionsDisplay = new google.maps.DirectionsRenderer();
//
//        directionsDisplay.setMap(map);
//        var waypnt=[];
//        var route=<?php echo json_encode($routes); ?>;
//        route.forEach(function(val){
//            if(!(val[3]==0 || val[3]==route.length-1)){
//                waypnt.push({
//                    location: new google.maps.LatLng(val[1],val[2]),
//                    stopover: true
//                });
//            }
//        });
//
//        var start = new google.maps.LatLng(<?php echo json_encode($startLat); ?>, <?php echo json_encode($startLong); ?>);
//        var end = new google.maps.LatLng(<?php echo json_encode($endLat); ?>, <?php echo json_encode($endLong); ?>);
//        var request = {
//            origin: start,
//            destination: end,
//            waypoints: waypnt,
//            optimizeWaypoints: true,
//            travelMode: google.maps.DirectionsTravelMode.DRIVING
//        };
//        directionsService.route(request, function (response, status) {
//            if (status == google.maps.DirectionsStatus.OK) {
//                directionsDisplay.setDirections(response);
//            }
//        });
//    }
//    else
//    {
//        var map = new google.maps.Map(document.getElementById('map'), {
//            zoom: 10,
//            center: new google.maps.LatLng(<?php echo json_encode($data['appt_data']->appt_lat); ?>, <?php echo json_encode($data['appt_data']->appt_long); ?>),
//            mapTypeId: google.maps.MapTypeId.ROADMAP
//        });
//
//        var infowindow = new google.maps.InfoWindow();
//
//        var marker, i;
//
//        directionsDisplay = new google.maps.DirectionsRenderer();
//
//        directionsDisplay.setMap(map);
//
//        var start = new google.maps.LatLng(<?php echo json_encode($data['appt_data']->appt_lat); ?>, <?php echo json_encode($data['appt_data']->appt_long); ?>);
//        var end = new google.maps.LatLng(<?php echo json_encode($data['appt_data']->drop_lat); ?>, <?php echo json_encode($data['appt_data']->drop_long); ?>);
//        var request = {
//            origin: start,
//            destination: end,
//            travelMode: google.maps.DirectionsTravelMode.DRIVING
//        };
//        directionsService.route(request, function (response, status) {
//            if (status == google.maps.DirectionsStatus.OK) {
//                directionsDisplay.setDirections(response);
//            }
//        });
//
//    }
    function openimg(imgid)
    {
        $('#modal-img').modal('show');
        var src=$(imgid).attr("src");
        console.log(src);
        $("#showimg").attr("src",src);
    }
</script>
<div class="modal fade fill-in" id="modal-img" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <i class="pg-close"></i>
    </button>
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                
            </div>
            <div class="modal-body">
                 <div class="row text-center" style="padding:20px;height:80vh;">   
                    <img src="" id="showimg" style="height:100%;">
                </div>  
            </div>
            <div class="modal-footer">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>