<?php

while (1) {
    require('Pubnub.php');

    $publish_key = "pub-c-56562a22-37c6-4c39-aad7-c740242df47f";
    $subscribe_key = "sub-c-980258f2-3b4e-11e4-8947-02ee2ddab7fe";

    $pubnub = new Pubnub($publish_key, $subscribe_key);

    $con = new Mongo();

    $freetaxi = $con->roadyo_live;

    $favourite = $freetaxi->selectCollection('favourite');
    $booking_route = $freetaxi->selectCollection('booking_route');

    $location = $freetaxi->selectCollection('location');

    $location->ensureIndex(array("location" => "2d"));

    $use = array('pubnub' => $pubnub, 'location' => $location, 'favourite' => $favourite, 'booking_route' => $booking_route, 'db' => $freetaxi);

    $pubnub->subscribe(array(
        "channel" => 'roadyo_alltypes',
        "callback" => function($message) use($use) {

            $a = (int) $message['message']['a'];

            $args = $message['message'];

            if ($a == 4) { //update driver location
                $cond = array("email" => strtolower($args['e_id']));

                $newdata = array('$set' => array("location" => array("longitude" => (double) $args['lg'], "latitude" => (double) $args['lt']), 'lastTs' => time()));

                $use['location']->update($cond, $newdata);

                if (isset($args['bid']) && (int) $args['bid'] > 0) {
                    $data = array("longitude" => (double) $args['lg'], "latitude" => (double) $args['lt']);
                    if (is_array($use['booking_route']->findOne(array('bid' => (int) $args['bid'])))) {
                        $use['booking_route']->update(
                                array('bid' => (int) $args['bid'], 'route' => array('$ne' => $data))
                                , array('$push' => array('route' => $data))
                                , array("upsert" => false)
                        );
                    } else {
                        $use['booking_route']->insert(array('bid' => (int) $args['bid'], 'route' => array($data)));
                    }
                }
            } else if ($a == 2) {//update driver about his status
                $cond = array("email" => strtolower($args['e_id']));

                $master = $use['location']->findOne($cond);

                if ($master['chn'] == 'qd_' . $args['d_id'])
                    $return = array('a' => 2, 's' => $master['status']);
                else
                    $return = array('a' => 2, 's' => 0);

                $pubRes[] = $use['pubnub']->publish(array(
                    'channel' => $args["chn"],
                    'message' => $return
                ));
            } else if ($a == 3) {//Acknowledge message from passenger to driver
                $cond = array("email" => strtolower($args['e_id']));

                $master = $use['location']->findOne($cond);

                $pubRes[] = $use['pubnub']->publish(array(
                    'channel' => $master["listner"],
                    'message' => $args
                ));
            } else if ($a == 11) {
                $cond = array("pasEmail" => strtolower($args['pid']));

                $favouritesData = $use['favourite']->find($cond);

                $driversArr = array();

                foreach ($favouritesData as $fav) {
                    $driversArr[] = $fav['driver'];
                }

                if (count($driversArr) <= 0)
                    $pubRes[] = $use['pubnub']->publish(array(
                        'channel' => $master["chn"],
                        'message' => array('a' => 11, 'flag' => 1)
                    ));

                $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
                    'geoNear' => 'location',
                    'near' => array(
                        (double) $args['ent_longitude'], (double) $args['ent_latitude']
                    ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137,
                    'query' => array('user' => array('$in' => $driversArr)))
                );

                $md_arr = $es = array();
//                    
                foreach ($resultArr['results'] as $res) {
                    $doc = $res['obj'];
                    $es[] = $doc['email'];
                    $md_arr[] = array('chn' => $doc['chn'], 'e' => $doc['email'], 'lt' => $doc['location']['latitude'], 'lg' => $doc['location']['longitude'], 'd' => ($res['dis']));
                }

                if (count($md_arr) > 0)
                    $pubRes[] = $use['pubnub']->publish(array(
                        'channel' => $master["chn"],
                        'message' => array('a' => 11, 'flag' => 0, 'masArr' => $md_arr, 'es' => $es)
                    ));
                else
                    $pubRes[] = $use['pubnub']->publish(array(
                        'channel' => $master["chn"],
                        'message' => array('a' => 11, 'flag' => 1)
                    ));
            } else if ($a == 1) { // send passenger all the drivers in a specific radius
                $found = $foundEmails = $types = $foundEs = $typesData = $foundNew = array();

                if ($args['st'] == '3') { // getting data for live drivers
                    $cond = array(
                        'geoNear' => 'vehicleTypes',
                        'near' => array(
                            (double) $args['lg'], (double) $args['lt']
                        ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137);

                    $resultArr1 = $use['db']->selectCollection('$cmd')->findOne($cond);

                    foreach ($resultArr1['results'] as $res) {
                        $doc = $res['obj'];

                        $types[] = (int) $doc['type'];

                        $typesData[$doc['type']] = array(
                            'type_id' => (int) $doc['type'],
                            'type_name' => $doc['type_name'],
                            'max_size' => (int) $doc['max_size'],
                            'basefare' => (float) $doc['basefare'],
                            'min_fare' => (float) $doc['min_fare'],
                            'price_per_min' => (float) $doc['price_per_min'],
                            'price_per_km' => (float) $doc['price_per_km'],
                            'type_desc' => $doc['type_desc']
                        );
                    }

                    $typesDataNew = array();

                    $types = array_filter(array_unique($types));
                    sort($types);

                    foreach ($types as $t) {
                        $typesDataNew[] = $typesData[$t];
                    }

                    $resultArr = $use['db']->selectCollection('$cmd')->findOne(array(
                        'geoNear' => 'location',
                        'near' => array(
                            (double) $args['lg'], (double) $args['lt']
                        ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137,
                        'query' => array('status' => 3))
                    );

                    foreach ($resultArr['results'] as $res) {
                        $doc = $res['obj'];

//                        $types[] = (int) $doc['type'];

                        if (count($foundEs[$doc['type']]) < 5)
                            $foundEs[$doc['type']][] = $doc['email'];

                        $found[$doc['type']][] = array('chn' => $doc['chn'], 'e' => $doc['email'], 'lt' => $doc['location']['latitude'], 'lg' => $doc['location']['longitude'], 'd' => ($res['dis']));
                    }
                } else { // scope for later booking 
                }

                $typesFiltered = array_unique(array_filter($types));

                foreach ($typesFiltered as $type) {
                    $es[] = array('tid' => $type, 'em' => (is_array($foundEs[$type])) ? $foundEs[$type] : array());
                    $masArr[] = array('tid' => $type, 'mas' => (is_array($found[$type])) ? $found[$type] : array());
                }

                if (count($typesDataNew) > 0)
                    $return = array('a' => 2, 'masArr' => $masArr, "tp" => $args['tp'], 'st' => $args['st'], 'flag' => 0, 'es' => $es, 'types' => $typesDataNew);
                else
                    $return = array('a' => 2, 'flag' => 1, 'types' => $typesDataNew);

                $pubRes[] = $use['pubnub']->publish(array(
                    'channel' => $args["chn"],
                    'message' => $return
                ));

//                print_r($args);
//                print_r($return);
            }
//            echo $a;

            $keepListening = true;
            return $keepListening;
        }
            ));
        }
?>